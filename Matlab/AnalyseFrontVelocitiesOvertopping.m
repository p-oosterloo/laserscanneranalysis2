function FrontVelocitiesAndOvertoppingResults = AnalyseFrontVelocitiesOvertopping(AnalysisInput)
% FrontVelocitiesAndOvertoppingResults = AnalyseFrontVelocitiesOvertopping(AnalysisInput)
%
% read in data based on RunUpResults (run-up analysis) structure and
% determine front velocities, wave overtopping discharge and volumes
% based on 2 methods (maximum volumes above virtual crest and front
% velocities multiplied with layer thicknesses)
%
% a-coordinate is defined as the distance along the slope
% x-coordinate is defined as the horizontal distance
% z-coordinate is defined as the height (vertically upward)
%
% input:
% AnalysisInput data structure, with fields:
%   .T                       = test time series (s);
%   .DateAndHour             = date and hour time series (datetime format)
%   .aDry                    = a-coordinates of dry slope (m)
%   .zDry                    = z-coordinates of dry slope (m)
%   .LayerThickness          = Timeseries of layer thickness (m);
%   .aRunUpAbsolute          = Run-up series in a-coordinates based on layer
%                             thickness (absolute) (m)
%   .zRunUpRelative          = Run-up series in z-coordinates based on
%                             layer thickness (relative) (m)
%   .LayerThicknessThreshold = layer thickness threshold for analysis (m)
%   .WaterLevel              = water level during measurement (constant) (m)
%   .WavePeriod              = mean wave period (or 0.8*peak period) (s)
%   .zCrestsAbsolute         = requested absolute virtual z-crest levels (m or m+NAP)
%   .RemoveLayerThicknessBias= toggle to remove -potential- bias in layer
%                              thickness ('y' or 'n')
%
% output:
% FrontVelocitiesAndOvertoppingResults data structure, with fields:
%   .T                         = test time series (s);
%   .DateAndHour               = date and hour time series (datetime format)
%   .aRunUpAbsoluteSmooth      = Smoothed run-up series in a-coordinates based on
%                                layer thickness (absolute) (m)
%   .zRunUpRelativeSmooth      = Smoothed run-up series in z-coordinates based on
%                                layer thickness (relative) (m)
%   .FrontVelocity             = front velocity timeseries (m/s)
%   .FrontVelocityLocs         = location IDs of front velocity peaks (-)
%   .FrontVelocityPeaks        = front velocity peaks (m/s)
%   .CrestIds                  = ids of virtual crest levels (-)
%   .zCrests                   = requested absolute virtual z-crest levels (m)
%   .Freeboards                = requested freeboard heights
%                                (=relative z-crest levels) (m)
%   .LayerThickness            = timeseries of layer thickness (m)
%   .VolumesBasedOnVolumes     = overtopping volumes timeseries based on max.
%                                volumes above virt. crest (time,crest) (m3/m)
%   .PeakVolumesBasedOnVolumes = maximum overtopping volume per wave timeseries
%                                based on max. volumes above virt. crest
%                                (time,crest) (m3/m)
%   .VolumeLocsBasedOnVolumes  = location IDs of maximum overtopping
%                                volumes per wave, based on max. volumes
%                                above virt. crest (-)
%   .VolumePeaksBasedOnVolumes = sorted maximum overtopping volumes per wave
%                                based on max. volumes above virt. crest (m3/m)
%   .MaxVolumeBasedOnVolumes   = maxima of overtopping volumes during whole
%                                storm per virtual crest level, based on
%                                max. volumes above virt. crest (m3/m)
%   .qBasedOnVolumes           = mean overtopping discharge per crest level,
%                                based on max. volumes above virt. crest (m3/s/m)
%   .VolumeLocsBasedOnFrontVelocities  = location IDs of maximum overtopping
%                                        volumes per wave, based on front vel.
%                                        * layer thickness (-)
%   .VolumePeaksBasedOnFrontVelocities = maximum overtopping volumes per
%                                        wave based on front vel.
%                                        * layer thickness (time,crest) (m3/m)
%   .MaxVolumeBasedOnFrontVelocities   = maxima of overtopping volumes during whole
%                                        storm per virtual crest level,
%                                        based on front vel. * layer thickness (m3/m)
%   .qSeriesBasedOnFrontVelocities     = overtopping discharge timeseries based
%                                        on front vel. * layer thickness (m3/s/m)
%   .qBasedOnFrontVelocities   = mean overtopping discharge per crest level, 
%                                based on front vel. * layer thickness (m3/s/m)
%   .WaterLevel                = water level during measurement (constant) (m)
%   .WavePeriod                = mean wave period (or 0.8*peak period) (s)
%   .Stormduration             = storm duration (s)
%   .LayerThicknessThreshold   = layer thickness threshold for analysis (m)
%   .RemoveLayerThicknessBias  = toggle to remove -potential- bias in layer
%                                thickness ('y' or 'n')
%
% Patrick Oosterlo, 07-2021
%
% Copyright 2021, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Load input
if isfield(AnalysisInput,'LayerThickness')
    LayerThickness = AnalysisInput.LayerThickness; end
if isfield(AnalysisInput,'aRunUpAbsolute')
    aRunUpAbsolute = AnalysisInput.aRunUpAbsolute; end
if isfield(AnalysisInput,'zRunUpRelative')
    zRunUpRelative = AnalysisInput.zRunUpRelative; end
if isfield(AnalysisInput,'T')
    T = AnalysisInput.T; end
if isfield(AnalysisInput,'DateAndHour')
    DateAndHour = AnalysisInput.DateAndHour; end
if isfield(AnalysisInput,'aDry')
    aDry = AnalysisInput.aDry; end
if isfield(AnalysisInput,'zDry')
    zDry = AnalysisInput.zDry; end
if isfield(AnalysisInput,'zCrestsAbsolute')
    zCrestsAbsolute = AnalysisInput.zCrestsAbsolute; end
if isfield(AnalysisInput,'WaterLevel')
    WaterLevel = AnalysisInput.WaterLevel; end
if isfield(AnalysisInput,'WavePeriod')
    WavePeriod = AnalysisInput.WavePeriod; end
if isfield(AnalysisInput,'LayerThicknessThreshold')
    LayerThicknessThreshold = AnalysisInput.LayerThicknessThreshold; end
if isfield(AnalysisInput,'RemoveLayerThicknessBias')
    RemoveLayerThicknessBias = AnalysisInput.RemoveLayerThicknessBias; end

%% Determine front velocities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The filter settings are based on previous experience, some tweaking%
%may be necessary!                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Smoothen the a-coordinate run-up data
aRunUpSmooth = movmedian(aRunUpAbsolute, [5 5],1,'omitnan');%11 pts
aRunUpSmooth = movmean(aRunUpSmooth, [10 10],1,'omitnan');%21 pts

%Smoothen the z-coordinate run-up data
zRunUpSmooth = movmedian(zRunUpRelative, [5 5],1,'omitnan');%11 pts
zRunUpSmooth = movmean(zRunUpSmooth, [10 10],1,'omitnan');%21 pts
%Fill up missing values (NaNs)
zRunUpSmooth = fillmissing(zRunUpSmooth,'linear');

%Determine front velocity
FrontVelocity = diff(aRunUpSmooth)./diff(T);
FrontVelocity = [NaN;FrontVelocity];%make equal length again

%Smoothen front velocity
FrontVelocitySmooth = movmedian(FrontVelocity,[5 5],1,'omitnan');%11 pts
FrontVelocitySmooth = movmean(FrontVelocitySmooth,[10 10],1,'omitnan');%21 pts

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%plot run-up and smoothed run-up
figure; hold on; grid on; box on;
plot(DateAndHour,aRunUpAbsolute);
plot(DateAndHour,aRunUpSmooth);
title('a-coordinate run-up versus smoothed a-coor run-up')
xlabel 'Date and Hour'
ylabel 'a-coordinate Run-up [m]'
legend('Raw a run-up','Smoothed a run-up')

prompt = {'Please check the plots. Do the signals seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%plot front velocity signal
figure; hold on; grid on; box on;
plot(DateAndHour,FrontVelocity)
plot(DateAndHour,FrontVelocitySmooth)
title('Front Velocity and Smoothed Front Velocity signals')
xlabel 'Date and Hour'
ylabel 'Front Velocity [m/s]'
legend('Raw front velocity','Smoothed front velocity')

prompt = {'Please check the plots. Do the signals seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

clear FrontVelocity

MedianScanFrequency = median(1./diff(T)); %determine median scan frequency

%Determine front velocity peaks, consider only peaks >0.5 m/s,
%peaks at 0.75*mean wave period distance from one another
[FrontVelocityPeaks,FrontVelocityLocs] = findpeaks(FrontVelocitySmooth,...
    'MinPeakHeight',0.5,'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);

%Plot front velocity and front velocity peaks
figure; hold on; grid on; box on;
plot(DateAndHour,FrontVelocitySmooth);
plot(DateAndHour(FrontVelocityLocs),FrontVelocityPeaks,'ro')
legend('Front velocity','Front velocity peaks')
title(['Front Velocity time signal, no. peaks = ',...
    num2str(length(FrontVelocityPeaks))])

prompt = {'Please check the plot. Do the results seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%% Determine overtopping based on maximum volumes above virtual crest
%Determine actual z-coordinates of crest levels
if exist('zCrestsAbsolute','var')==1
    for i = 1:1:length(zCrestsAbsolute)
        CrestIds(i) = find(abs(zDry-zCrestsAbsolute(i))==min(abs(...
            zDry-zCrestsAbsolute(i))),1,'first');
    end
else
    display('Error: provide z coordinates of virtual crest levels!')
end
zCrests = zDry(CrestIds);%actual virtual z-crest levels
Freeboards = zCrests-WaterLevel;%Freeboards

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The filter settings are based on previous experience, some tweaking%
%may be necessary!                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% LayerThicknessSmooth = movmedian(LayerThickness,[2 2],1,'omitnan');%5 pts=0.1 s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%If toggle is turned on, remove -potential- bias in layer thickness
if strcmp(RemoveLayerThicknessBias,'y')==1
    LayerThickness=LayerThickness-LayerThicknessThreshold;
end 
%set layer thickness <0 to zero layer thickness
LayerThickness(LayerThickness<0)=0;
%set NaN to zero layer thickness
LayerThickness(isnan(LayerThickness))=0;

%plot example of layer thicknesses at the median of the crest levels
figure; hold on; grid on; box on;
plot(DateAndHour,LayerThickness(:,median(CrestIds)))
xlabel 'Date and Hour'; ylabel 'Layer thickness [m]'
title(['Layer thickness at crest at z = ',num2str(median(zCrests)),' m'])

prompt = {'Please check the plot. Do the results seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%Find volumes above virtual crest levels by integrating layer thickness
%along a-coordinates
for i = 1:1:length(zCrests)
    Volumes(:,i) = trapz(aDry(CrestIds(i):end),...
        LayerThickness(:,CrestIds(i):end),2);
end

%generate some matrices with zeroes
PeakVolumes = zeros(size(Volumes));
VolumePeaks = zeros(size(Volumes));
VolumeLocs = zeros(size(Volumes));
%set to NaNs instead of zeroes before filling them
PeakVolumes(PeakVolumes==0)=NaN;
VolumePeaks(VolumePeaks==0)=NaN;
VolumeLocs(VolumeLocs==0)=NaN;

%Determine peak volumes, peaks at 0.75*mean wave period distance from one another
for i = 1:1:length(zCrests)
    [Vpks,Vlocs] = findpeaks(Volumes(:,i),...
        'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);%find peaks
    VolumePeaks(1:length(Vpks),i) = Vpks;%save peaks
    VolumeLocs(1:length(Vlocs),i) = Vlocs;%save peak locations
    PeakVolumes(Vlocs,i)=Vpks;%save peak volumes timeseries
end

%Plot example of volumes and peak volumes
figure; hold on; grid on; box on;
plot(DateAndHour,Volumes(:,round(length(zCrests)/2)))
plot(DateAndHour,PeakVolumes(:,round(length(zCrests)/2)),'ro')
title(['Overtopping volumes above virtual crest at z = ',...
    num2str(median(zCrests)),' m. No. peaks = ',...
    num2str(sum(~isnan(...
    VolumePeaks(:,round(length(zCrests)/2)))))]);
xlabel 'Date and Hour [s]'; ylabel 'volume [m^3/m]';
legend('Volume','Max. volume')

prompt = {'Please check the plot. Do the data seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%Plot example of volumes along slope using layer thickness
figure; hold on; grid on; box on;
ids = find(~isnan(VolumeLocs(:,round(length(CrestIds)/2))));
plot(aDry(median(CrestIds):end),LayerThickness(...
    VolumeLocs(ids,round(length(CrestIds)/2)),median(CrestIds):end))
title 'Maximum overtopping volumes along the slope (a-coordinates)'
xlabel 'a-coordinate [m]'
ylabel 'Layer Thickness [m]'

prompt = {'Please check the plot. Do the results seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%Determine (storm) duration
StormDuration = T(end)-T(1);

%Determine maximum overtopping volume and mean overtopping discharge
MaxVolumes = max(PeakVolumes,[],1,'omitnan'); %in m^3
q = sum(PeakVolumes,1,'omitnan')/StormDuration; %in m^3/s/m

%% Determine overtopping based on front velocities * layer thicknesses
%Determine q-discharge time series
qSeries = FrontVelocitySmooth.*LayerThickness(:,CrestIds);

%Plot q-discharge time series
figure; hold on; grid on; box on;
plot(DateAndHour,qSeries(:,round(length(zCrests)/2)))
title 'q-discharge time series (based on velocity*layer thickness)'
xlabel 'Date and Hour'
ylabel 'q [m3/s/m]'

prompt = {'Please check the plot. Do the data seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%Neglect discharges <0 (run-down)
qSeries(qSeries<0)=0;
%Determine mean overtopping discharge
q2 = mean(qSeries,1,'omitnan');%in m3/s/m

%Determine peak volumes
VolumePeaks2 = zeros(size(qSeries));
VolumeLocs2 = zeros(size(qSeries));
VolumePeaks2(VolumePeaks2==0)=NaN;
VolumeLocs2(VolumeLocs2==0)=NaN;

for i = 1:1:length(zCrests)
    a = diff(qSeries(:,i));%derivative of q
    a = [0;a];%make length equal to time series again
    b = find(a~=0);%find derivative of q = nonzero, so where a discharge
    %starts or stops to occur
    c = diff(b);%find the distance between the locations where this is true,
    %to be able to separate the individual volumes
    c = [NaN;c];%make length equal to time series again
    d = find(c>1);%find gaps in location ids to be able to separate volumes
    
    if length(c)<=1 %No discharge/volume found
        
    elseif isempty(d) & length(c)>1 %1 volume/discharge found
        %Determine locations of start of volumes
        VolumeLocs2(1:length(b(d))+1,i) = [b(1)-1;(b(d)-1)];
        
        %Determine peak volumes by integrating q series over time
        j=1;
        VolumePeaks2(j,i) = trapz(T(b(1)-1:b(end)),...
            qSeries(b(1)-1:b(end),i));
        
    else %Multiple discharges/volumes found
        %Determine locations of start of volumes
        VolumeLocs2(1:length(b(d))+1,i) = [b(1)-1;(b(d)-1)];
        
        %Determine peak volumes by integrating q series over time
        for j = 1:1:length(d)+1
            if j==1
                if length(b(1)-1:b(d(j)-1))==1
                    VolumePeaks2(j,i) =  NaN;
                else
                    VolumePeaks2(j,i) = trapz(T(b(1)-1:b(d(1)-1)),...
                        qSeries(b(1)-1:b(d(1)-1),i));
                end
            elseif j==length(d)+1
                if length(b(d(j-1))-1:b(end))==1
                    VolumePeaks2(j,i) =  NaN;
                else
                    VolumePeaks2(j,i) = trapz(T(b(d(j-1))-1:b(end)),...
                        qSeries(b(d(j-1))-1:b(end),i));
                end
            else
                if length(b(d(j-1))-1:b(d(j)-1))==1
                    VolumePeaks2(j,i) =  NaN;
                else
                    VolumePeaks2(j,i) = trapz(T(b(d(j-1))-1:b(d(j)-1)),...
                        qSeries(b(d(j-1))-1:b(d(j)-1),i));
                end
            end
        end
    end
end

%Plot example of q-series and found volumes
figure; hold on; grid on; box on;
ids = VolumeLocs2(...
    ~isnan(VolumeLocs2(:,round(length(zCrests)/2))),round(length(zCrests)/2));
plot(DateAndHour,qSeries(:,round(length(zCrests)/2)))
plot(DateAndHour(ids)',VolumePeaks2(1:length(ids),round(length(zCrests)/2)),'ro')
title 'q-discharge time series and maximum overtopping volumes'
xlabel 'Date and Hour'
ylabel 'q [m3/s/m], V [m3/m]'
legend('Overtopping discharge','Max. volume')

prompt = {'Please check the plot. Do the data seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

MaxVolumes2 = max(VolumePeaks2,[],1,'omitnan');%determine maximum volumes

%% Generate and save output
%Combine all output into a FrontVelocitiesAndOvertoppingResults structure
FrontVelocitiesAndOvertoppingResults.T = T;
FrontVelocitiesAndOvertoppingResults.DateAndHour = DateAndHour;
FrontVelocitiesAndOvertoppingResults.aRunUpAbsoluteSmooth = aRunUpSmooth;
FrontVelocitiesAndOvertoppingResults.zRunUpRelativeSmooth = zRunUpSmooth;
FrontVelocitiesAndOvertoppingResults.FrontVelocity = FrontVelocitySmooth;
FrontVelocitiesAndOvertoppingResults.FrontVelocityLocs = FrontVelocityLocs;
FrontVelocitiesAndOvertoppingResults.FrontVelocityPeaks = FrontVelocityPeaks;
FrontVelocitiesAndOvertoppingResults.CrestIds = CrestIds;
FrontVelocitiesAndOvertoppingResults.zCrests = zCrests;
FrontVelocitiesAndOvertoppingResults.Freeboards = Freeboards;
FrontVelocitiesAndOvertoppingResults.LayerThickness = LayerThickness;
FrontVelocitiesAndOvertoppingResults.VolumesBasedOnVolumes = Volumes;
FrontVelocitiesAndOvertoppingResults.PeakVolumesBasedOnVolumes = PeakVolumes;
FrontVelocitiesAndOvertoppingResults.VolumeLocsBasedOnVolumes = VolumeLocs;
FrontVelocitiesAndOvertoppingResults.VolumePeaksBasedOnVolumes = VolumePeaks;
FrontVelocitiesAndOvertoppingResults.MaxVolumeBasedOnVolumes = MaxVolumes;
FrontVelocitiesAndOvertoppingResults.qBasedOnVolumes = q;
FrontVelocitiesAndOvertoppingResults.VolumeLocsBasedOnFrontVelocities = VolumeLocs2;
FrontVelocitiesAndOvertoppingResults.VolumePeaksBasedOnFrontVelocities = VolumePeaks2;
FrontVelocitiesAndOvertoppingResults.MaxVolumeBasedOnFrontVelocities = MaxVolumes2;
FrontVelocitiesAndOvertoppingResults.qSeriesBasedOnFrontVelocities = qSeries;
FrontVelocitiesAndOvertoppingResults.qBasedOnFrontVelocities = q2;
FrontVelocitiesAndOvertoppingResults.WaterLevel = WaterLevel;
FrontVelocitiesAndOvertoppingResults.WavePeriod = WavePeriod;
FrontVelocitiesAndOvertoppingResults.Stormduration = StormDuration;
FrontVelocitiesAndOvertoppingResults.LayerThicknessThreshold = LayerThicknessThreshold;
FrontVelocitiesAndOvertoppingResults.RemoveLayerThicknessBias = RemoveLayerThicknessBias;

%Prompt to save data
prompt = {'Save data? (y/n):'};
title1 = 'Save data?';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    [filename1, filepath1] = uiputfile('*.mat', 'Save the front velocity and overtopping file:');
    FileName = fullfile(filepath1, filename1);
    save(FileName, 'FrontVelocitiesAndOvertoppingResults', '-v7.3');
end
