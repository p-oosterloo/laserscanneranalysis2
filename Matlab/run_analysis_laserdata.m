% Run through all the steps of the analysis of the laser scanner data,
% in this case SICK LMS511 laser scanners using SOPAS ET
%
% Patrick Oosterlo, 01-2021
%
% Copyright 2021, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Step 1: Convert SOPAS *.data file to *.csv file within SOPAS
% 1. Open *.data file in SOPAS data recorder
% 2. Press the export button
% 3. Export only selected variables (2 times ScanData) or certain time range
% 4. Export to *.csv, create own file for each variable, comma or semicolon
%    separated and period decimal separator

%% Step 2: Load *.csv file and convert relevant data to *.mat file
% Enter filename and Nmax, the max. number of lines (dependent on pc memory)
data = ReadSopasCsv(...
'O:\2_ProcessedData\Testdagen\testIRlamp_LMS5xx_FieldEval_PRO_(SN_19400127)_ScanData_20201015204839_20201015205010.csv',...
1e8);

%% Step 3: Determine dry dike slope
% No 'automatic' script exists for this step, this is more manual labour.
% A similar procedure as the file DetermineDrySlope.m could be used:
% 1. Generate *.mat files with the dry slope at low tides (so recorded
%    outside the storm peaks)
% 2. Load these files and apply median filters on the data
% 3. Merge the data of different low tides and determine the median values
% 4. Determine the maximum differences between the different dry slopes
% 5. Only for Ciara: remove the influence of the painted grid lines
% 6. Save the data

edit DetermineDrySlope

%% Step 4: Determine orientation angle of laser
%  at which scan angle does the laser scan straight down (theta=0)?

% Load calibration file for laser 1 or 2
% this file should have an object on the slope, directly under the laser
% scanner!
load('CiaraLaser27ObjectCalibration0m.mat');

% Prepare input and determine orientation angle
%Measured distances R
AnalysisInput.R = data.R;
%Angle at which laser starts scanning
AnalysisInput.StartAngle = data.StartAngle; 
%Angular resolution of laser
AnalysisInput.AngleRes = data.AngleRes; 
%Aperture angle of the laser scanner
AnalysisInput.ApertureAngle = 190; 

OrientationAngle = DetermineOrientationAngle(AnalysisInput);

%% Step 5: First part of dike slope calibration
%  Determine differences between GPS-measured dike slope and laser slope
%  Determine CorrectionAngle for rotation of laser line around the y-axis

% Load dry slope file for laser 1 or 2
% this file should have no objects on the slope!
load('CiaraLaser27DrySlopeFinalnewRemovedGridLines.mat');

%GPS-measured x-coordinates (horizontal)
xend = [0;0.960;1.926;2.888;3.848;4.817;5.780;6.748;7.716;8.687;9.658;...
    10.628;11.606;12.586;13.564;14.543;15.521;16.501;17.481;18.464;19.451];
%GPS-measured z-coordinates (height, vertically upward)
zend = [1.960;2.240;2.500;2.770;3.050;3.300;3.570;3.820;4.070;4.310;4.550;...
    4.790;5.000;5.200;5.410;5.610;5.820;6.020;6.220;6.400;6.560];
%GPS-measured a-coordinates (distance along slope, lines on slope)
aend = [0;1;2.000;3.000;4.000;5.000;6.000;7.000;8.000;9.000;10.000;11.000;...
    12.000;13.000;14.000;15.000;16.000;17.000;18.000;19.000;20.000];

%Prepare input and determine correction angle
%Measured distances R
AnalysisInput.R = data.R;
% AnalysisInput.R2 = data.R2; %in case multiple echoes are used
% AnalysisInput.R3 = data.R3;
% AnalysisInput.R4 = data.R4;
% AnalysisInput.R5 = data.R5;
%Laser reflectance RSSI
AnalysisInput.RSSI = data.RSSI;
% AnalysisInput.RSSI2 = data.RSSI2;
% AnalysisInput.RSSI3 = data.RSSI3;
% AnalysisInput.RSSI4 = data.RSSI4;
% AnalysisInput.RSSI5 = data.RSSI5;
%Angle at which laser starts scanning
AnalysisInput.StartAngle = AnalysisInput.StartAngle;
%Angular resolution of laser
AnalysisInput.AngleRes = AnalysisInput.AngleRes;
%Aperture angle of the laser scanner
AnalysisInput.ApertureAngle = AnalysisInput.ApertureAngle; 
%Slant angle of laser
AnalysisInput.SlantAngle = 37.562;%26.043; 
%x-coordinate of laser
AnalysisInput.xCoordinateLaser = 11.606; 
%a-coordinate of laser
AnalysisInput.aCoordinateLaser = 12; 
%height of laser above slope
AnalysisInput.HeightLaser = 6.036;%5.550; 
%local dike slope angle at laser (pole)
AnalysisInput.SlopeAngleAtLaser = 11.345; 
%GPS-measured x-coordinates
AnalysisInput.xCoordinatesMeasured = xend; 
%GPS-measured z-coordinates
AnalysisInput.zCoordinatesMeasured = zend; 
%GPS-measured a-coordinates
AnalysisInput.aCoordinatesMeasured = aend; 
%Orientation of laser
AnalysisInput.OrientationAngle = OrientationAngle;
%Laser pole can be vertical or perpendicular to the slope
AnalysisInput.PoleOrientation = 'Vertical'; 
%a-coordinates of area of interest, relative to the laser pole
%a-coordinate, so assume laser pole a=0 for this parameter. This should be
%based on the expected maximum run-up height. This limits the range
%considered in the analysis to only the area where run-up actually occurs.
%This can be used to improve the results of the analyses. Here: run-up from
%the toe (-12) to somewhat above the laser pole on the slope (0.5 - 1.5)
AnalysisInput.aAreaOfInterest = [-12 0.5];%[-12 1.5];
%the same for the x-coordinates, relative to pole, so assume pole x=0
AnalysisInput.xAreaOfInterest = [-11.6060 0.49];%[-11.6060 1.4687];
%Maximum distance that laser scanner can scan (in m)
AnalysisInput.MaxScanDistanceLaser = 80;

%Note: Analysis for perpendicular pole might need to be fixed!
[CorrectionAngle,deltaSlope,xDry,aDry,zDry,RSSIDry,...
    AreaOfInterestId1,AreaOfInterestId2] = ...
    SlopeCalibration(AnalysisInput);

%% Step 6: Second part of dike slope calibration
%  Calibration with object on slope at known location

%Load file for scanner 1 or 2, containing one or more objects
%placed on the dry slope
load('CiaraLaser27ObjectCalibration0m.mat');

%GPS-measured x-coordinates (horizontal)
xend = [0;0.960;1.926;2.888;3.848;4.817;5.780;6.748;7.716;8.687;9.658;...
    10.628;11.606;12.586;13.564;14.543;15.521;16.501;17.481;18.464;19.451];
%GPS-measured z-coordinates (height, vertically upward)
zend = [1.960;2.240;2.500;2.770;3.050;3.300;3.570;3.820;4.070;4.310;4.550;...
    4.790;5.000;5.200;5.410;5.610;5.820;6.020;6.220;6.400;6.560];
%GPS-measured a-coordinates (distance along slope, lines on slope)
aend = [0;1;2.000;3.000;4.000;5.000;6.000;7.000;8.000;9.000;10.000;11.000;...
    12.000;13.000;14.000;15.000;16.000;17.000;18.000;19.000;20.000];

%Prepare input and calibrate slope
%Measured distances R
AnalysisInput.R = data.R;
% AnalysisInput.R2 = data.R2; %in case multiple echoes are used
% AnalysisInput.R3 = data.R3;
% AnalysisInput.R4 = data.R4;
% AnalysisInput.R5 = data.R5;
%Angle at which laser starts scanning
AnalysisInput.StartAngle = AnalysisInput.StartAngle;
%Angular resolution of laser
AnalysisInput.AngleRes = AnalysisInput.AngleRes;
%Aperture angle of the laser scanner
AnalysisInput.ApertureAngle = AnalysisInput.ApertureAngle; 
%Slant angle of laser
AnalysisInput.SlantAngle = AnalysisInput.SlantAngle;
%x-coordinate of laser
AnalysisInput.xCoordinateLaser = AnalysisInput.xCoordinateLaser;
%a-coordinate of laser
AnalysisInput.aCoordinateLaser = AnalysisInput.aCoordinateLaser;
%height of laser above slope
AnalysisInput.HeightLaser = AnalysisInput.HeightLaser;
%local dike slope angle at laser (pole)
AnalysisInput.SlopeAngleAtLaser = AnalysisInput.SlopeAngleAtLaser;
%GPS-measured x-coordinates
AnalysisInput.xCoordinatesMeasured = xend;
%GPS-measured z-coordinates
AnalysisInput.zCoordinatesMeasured = zend;
%GPS-measured a-coordinates
AnalysisInput.aCoordinatesMeasured = aend;
%Orientation of laser
AnalysisInput.OrientationAngle = AnalysisInput.OrientationAngle;
%Laser pole can be vertical or perpendicular to the slope
AnalysisInput.PoleOrientation = AnalysisInput.PoleOrientation;
%a-coordinates of area of interest, relative to the laser pole
%a-coordinate, so assume laser pole a=0 for this parameter. This should be
%based on the expected maximum run-up height. This limits the range
%considered in the analysis to only the area where run-up actually occurs.
%This can be used to improve the results of the analyses. 
AnalysisInput.aAreaOfInterest = AnalysisInput.aAreaOfInterest;
%the same for the x-coordinates
AnalysisInput.xAreaOfInterest = AnalysisInput.xAreaOfInterest;
%Correction angle for a rotation around the y-axis
AnalysisInput.CorrectionAngle = CorrectionAngle;
%Maximum distance that laser scanner can scan (in m)
AnalysisInput.MaxScanDistanceLaser = AnalysisInput.MaxScanDistanceLaser;
%a-coordinate of object(s), relative to pole a-coordinate!
AnalysisInput.aObjectLocations = [0];
%Initial width of laser beam (m)
AnalysisInput.WidthLaserBeam = 0.013;
%Divergence of laser beam (rad)
AnalysisInput.DivergenceLaserBeam = 0.0047;

%Note: Analysis for perpendicular pole might need to be fixed!
[aLaserObjectLocations,xLaserObjectLocations,deltaObjects,LaserResolutions,...
    LaserFootprints] = ObjectCalibration(AnalysisInput);

%% Step 7: Determine layer thickness and run-up
%Load file with run-up for scanner 1 or 2
load('CiaraPeak1Laser27newRemovedGridLines.mat');

%GPS-measured x-coordinates (horizontal)
xend = [0;0.960;1.926;2.888;3.848;4.817;5.780;6.748;7.716;8.687;9.658;...
    10.628;11.606;12.586;13.564;14.543;15.521;16.501;17.481;18.464;19.451];
%GPS-measured z-coordinates (height, vertically upward)
zend = [1.960;2.240;2.500;2.770;3.050;3.300;3.570;3.820;4.070;4.310;4.550;...
    4.790;5.000;5.200;5.410;5.610;5.820;6.020;6.220;6.400;6.560];
%GPS-measured a-coordinates (distance along slope, lines on slope)
aend = [0;1;2.000;3.000;4.000;5.000;6.000;7.000;8.000;9.000;10.000;11.000;...
    12.000;13.000;14.000;15.000;16.000;17.000;18.000;19.000;20.000];

%Prepare input and determine layer thickness and run-up
%Measured distances R
AnalysisInput.R = data.R;
% AnalysisInput.R2 = data.R2; %in case multiple echoes are used
% AnalysisInput.R3 = data.R3;
% AnalysisInput.R4 = data.R4;
% AnalysisInput.R5 = data.R5;
%Laser reflectance RSSI
AnalysisInput.RSSI = data.RSSI;
% AnalysisInput.RSSI2 = data.RSSI2;
% AnalysisInput.RSSI3 = data.RSSI3;
% AnalysisInput.RSSI4 = data.RSSI4;
% AnalysisInput.RSSI5 = data.RSSI5;
%Angle at which laser starts scanning
AnalysisInput.StartAngle = data.StartAngle;
%Angular resolution of laser
AnalysisInput.AngleRes = AnalysisInput.AngleRes;
%Aperture angle of the laser scanner
AnalysisInput.ApertureAngle = AnalysisInput.ApertureAngle; 
%Slant angle of laser
AnalysisInput.SlantAngle = AnalysisInput.SlantAngle;
%x-coordinate of laser
AnalysisInput.xCoordinateLaser = AnalysisInput.xCoordinateLaser;
%a-coordinate of laser
AnalysisInput.aCoordinateLaser = AnalysisInput.aCoordinateLaser;
%height of laser above slope
AnalysisInput.HeightLaser = AnalysisInput.HeightLaser;
%local dike slope angle at laser (pole)
AnalysisInput.SlopeAngleAtLaser = AnalysisInput.SlopeAngleAtLaser;
%GPS-measured x-coordinates
AnalysisInput.xCoordinatesMeasured = xend;
%GPS-measured z-coordinates
AnalysisInput.zCoordinatesMeasured = zend;
%GPS-measured a-coordinates
AnalysisInput.aCoordinatesMeasured = aend;
%Orientation of laser
AnalysisInput.OrientationAngle = AnalysisInput.OrientationAngle;
%Laser pole can be vertical or perpendicular to the slope
AnalysisInput.PoleOrientation = AnalysisInput.PoleOrientation;
%Correction angle for a rotation around the y-axis
AnalysisInput.CorrectionAngle = CorrectionAngle;
%Maximum distance that laser scanner can scan (in m)
AnalysisInput.MaxScanDistanceLaser = AnalysisInput.MaxScanDistanceLaser;
%The area of interest was determined previously, so now only the resulting
%AreaOfInterestId1 and AreaOfInterestId2 are necessary as input
AnalysisInput.AreaOfInterestId1 = AreaOfInterestId1;
AnalysisInput.AreaOfInterestId2 = AreaOfInterestId2;
%Time since start of measurement (s)
AnalysisInput.T = data.T;
%Actual date and time (datetime format)
AnalysisInput.DateAndHour = data.DateAndHour;
%x-coordinates of dry slope
AnalysisInput.xDry = xDry;
%a-coordinates of dry slope
AnalysisInput.aDry = aDry;
%z-coordinates of dry slope
AnalysisInput.zDry = zDry;
%RSSI-values of dry slope
AnalysisInput.RSSIDry = RSSIDry;
%Requested time period for analysis, in cell with strings and in American
%format (mm-dd-yyy hh:mm:ss)
AnalysisInput.TimePeriod = {'02-10-2020 11:20:00' '02-10-2020 12:20:00'};% {'02-11-2020 23:50:00' '02-12-2020 01:10:00'};%
%Layer thickness threshold to be used in analysis (m)
AnalysisInput.LayerThicknessThreshold = 0.03;
%Number of straight NaNs after which no valid layer thickness may occur, to
%be used in layer thickness analysis (-) (e.g. 10 or 20)
AnalysisInput.NaNMaxLT = 20;
%RSSI threshold to be used in analysis (-)
AnalysisInput.RSSIThreshold = 5;
%Number of straight NaNs after which no valid RSSI value may occur, to
%be used in analysis (-) (e.g. 10 or 20)
AnalysisInput.NaNMaxRSSI = 12;
%Still water level during storm peak (m) or (m+NAP)
AnalysisInput.WaterLevel = 2.93;%3.03;2.93;
%Mean wave period (or as an estimate, 0.8*peak period)
AnalysisInput.WavePeriod = 3.7;

%Note: Analysis for layer thickness needs to be fixed!
%Note: Analysis for perpendicular pole might need to be fixed!
RunUpResults = AnalyseRunUp(AnalysisInput);

%Options for improving the run-up results:
% 1: Cut upper part of slope from data (AreaOfInterest)
% 2: Adjust rain/person filter to smaller number of NaNs (NaNMax)
% 3: Adjust layer thickness threshold (last resort)

%% Step 8: Determine front velocities, overtopping volumes and discharges
%Load RunUpResults *.mat file for laser scanner 1 or 2
load('CiaraPeak1Laser27RunUpResultsnewupdatedscripts.mat');

%Prepare input and determine front velocity, overtopping volumes & discharges
%Layer thickness series (time,space) (m)
AnalysisInput.LayerThickness = RunUpResults.LayerThickness;
%Absolute run-up timeseries in a-coordinates (m)
AnalysisInput.aRunUpAbsolute = RunUpResults.aRunUpAbsolute;
%Relative run-up timeseries in z-coordinates (m)
AnalysisInput.zRunUpRelative = RunUpResults.zRunUpRelative;
%Time since start of measurement (s)
AnalysisInput.T = RunUpResults.T;
%Actual date and time (datetime format)
AnalysisInput.DateAndHour = RunUpResults.DateAndHour;
%a-coordinates of dry slope
AnalysisInput.aDry = RunUpResults.aDry;
%z-coordinates of dry slope
AnalysisInput.zDry = RunUpResults.zDry;
%Layer thickness threshold to be used in analysis (m)
AnalysisInput.LayerThicknessThreshold = RunUpResults.LayerThicknessThreshold;
%Still water level during storm peak (m) or (m+NAP)
AnalysisInput.WaterLevel = RunUpResults.WaterLevel;
%Mean wave period (or as an estimate, 0.8*peak period)
AnalysisInput.WavePeriod = RunUpResults.WavePeriod;
%Absolute virtual z-crest levels (4.4 and 5.3 m+NAP are overtopping boxes)
AnalysisInput.zCrestsAbsolute = [3.25 3.5 3.57 3.75 4 4.25 4.31 4.4 4.5 4.75 5 5.25 5.3];
%Toggle to remove -potential- bias in layer thickness ('y' or 'n')
AnalysisInput.RemoveLayerThicknessBias = 'y';

FrontVelocitiesAndOvertoppingResults = AnalyseFrontVelocitiesOvertopping(AnalysisInput)

%Options for improving the front velocity and overtopping results:
% 1: Toggle removal of bias in layer thickness on/off
% 2: Smoothen run-up and/or layer thickness signals
% 3: Adjust layer thickness threshold (last resort)

%% Step 9: Determine wave (run-up) peak period and angle of incidence
%Load FrontVelocitiesAndOvertoppingResults *.mat file for laser scanner 1 or
%two files, for laser scanner 1 and 2
%The angle of incidence can only be determined based on 2 laser run-up
%signals.
data1 = load('CiaraPeak1Laser21FrontVelocitiesAndOvertoppingResults.mat');
data2 = load('CiaraPeak1Laser27FrontVelocitiesAndOvertoppingResults.mat');

%Prepare input and determine wave (run-up) peak period and angle of
%incidence
%Time since start of measurement (s)
AnalysisInput.T = data1.FrontVelocitiesAndOvertoppingResults.T;
%Actual date and time (datetime format)
AnalysisInput.DateAndHour = data1.FrontVelocitiesAndOvertoppingResults.DateAndHour;
%Smoothed run-up timeseries in relative z-coordinates, laser 1
AnalysisInput.zRunUpRelativeSmooth1 = data1.FrontVelocitiesAndOvertoppingResults.zRunUpRelativeSmooth;
%Smoothed run-up timeseries in relative z-coordinates, laser 2
%This command is optional, without it, no angle of indicence is calculated,
%only the wave (run-up) peak period
AnalysisInput.zRunUpRelativeSmooth2 = data2.FrontVelocitiesAndOvertoppingResults.zRunUpRelativeSmooth;
%Still water level during storm peak (m) or (m+NAP)
AnalysisInput.WaterLevel = data1.FrontVelocitiesAndOvertoppingResults.WaterLevel;
%Bottom level at toe of dike (m) or (m+NAP)
AnalysisInput.BottomLevel = 0.65;
%Distance between laser scanner scan lines (m)
AnalysisInput.DistanceBetweenScanLines = 1.972;

AngleOfIncidenceResults = AnalyseAngleOfIncidence(AnalysisInput);

%Options for improving the front velocity and overtopping results:
% 1: Smoothen run-up and/or layer thickness signals
% 2: Edit smoothing of time lag spectrum within analysis script (last
% resort)
