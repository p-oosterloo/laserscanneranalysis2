function [CorrectionAngle,deltaSlope,xDry,aDry,zDry,RSSIDry,...
    AreaOfInterestId1,AreaOfInterestId2] = SlopeCalibration(AnalysisInput)
% [CorrectionAngle,deltaSlope,xDry,aDry,zDry,RSSIDry,...
%   AreaOfInterestId1,AreaOfInterestId2] = SlopeCalibration(AnalysisInput)
%
% read in data from laser scanner *.mat file containing the dry slope
% without any objects or obstacles, and first determine the CorrectionAngle,
% a correction for a rotation around the y-axis. Then determine the
% differences between the measured dike slope (e.g. with measurement tape)
% and the slope as determined by the laser (deltaSlope)
%
% The combined effects of an irregular and convex dike slope, of the slight
% rotation around the z-axis of the laser scanners in order to get the
% laser lines run straight along the slope and at a constant distance from
% each other, and the influence of slight errors in the dimensions and
% orientation of the system, result in a rotation around the y-axis after
% converting the measured distances to cartesian coordinates.
% In order to account for this, the coordinates are rotated around the
% y-axis using a rotation matrix. The exact rotation is determined by
% fitting to the measured slope by using a least-squares method.
%
% a-coordinate is defined as the distance along the slope
% x-coordinate is defined as the horizontal distance
% z-coordinate is defined as the height (vertically upward)
%
% Note: the *.mat file needs to contain a stationary and uniform situation,
% so no temporal or spatial changes in the dry slope may occur!
%
% input:
% AnalysisInput data structure, with fields:
%   .R [R2,R3,R4,R5]       = radial distance from scanner (m) [for all echoes]
%   .RSSI [RSSI2,RSSI3,RSSI4,RSSI5] = RSSI data
%                                     (reflected signal intensity) (-)
%   .StartAngle            = start angle (deg.)
%   .AngleRes              = angle resolution (deg.)
%   .ApertureAngle         = aperture angle of laser scanner (deg.)
%   .SlantAngle            = slant angle of scanner (deg.)
%   .xCoordinateLaser      = x-coordinate (horizontal) of scanner (m)
%   .aCoordinateLaser      = a-coordinate (along slope) of scanner (m)
%   .HeightLaser           = height of scanner above slope (m)
%   .SlopeAngleAtLaser     = angle of dike slope at scanner (deg.)
%   .xCoordinatesMeasured  = measured x-coordinates (horizontal)
%                           (e.g. with measurement tape) (m)
%   .zCoordinatesMeasured  = measured z-coordinates (vertical)
%                           (e.g. with measurement tape) (m)
%   .aCoordinatesMeasured  = measured a-coordinates (along slope)
%                           (e.g. with measurement tape) (m)
%   .OrientationAngle      = angle at which scanner scans straight down
%                           (theta=0) (deg.)
%   .PoleOrientation       = orientation of laser pole, 'Vertical' or
%                          'Perpendicular'
%   .aAreaOfInterest       = a-coordinates of area of interest, relative to
%                           pole a-coordinate (e.g. [-10 10])
%   .xAreaOfInterest       = x-coordinates of area of interest, relative to
%                           pole x-coordinate (e.g. [-10 10])
%   .MaxScanDistanceLaser  = maximum distance that laser can scan (m)
%
% output:
%   CorrectionAngle     = correction angle for rotation around y-axis (deg)
%   deltaSlope          = max. difference between measured dike slope and
%                        laser slope (m)
%   xDry                = x-coordinates (horizontal) of dry slope (m)
%   aDry                = a-coordinates (along slope) of dry slope (m)
%   zDry                = z-coordinates (vertical) of dry slope (m)
%   RSSIDry             = RSSI values of dry slope (-)
%   AreaOfInterestId1   = ID that indicates where the area of interest starts;
%   AreaOfInterestId2   = ID that indicates where the area of interest ends;
%
% Patrick Oosterlo, 12-2020
%
% Copyright 2020, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Load input
if isfield(AnalysisInput,'R'); R = AnalysisInput.R; end
if isfield(AnalysisInput,'R2'); R2 = AnalysisInput.R2; end
if isfield(AnalysisInput,'R3'); R3 = AnalysisInput.R3; end
if isfield(AnalysisInput,'R4'); R4 = AnalysisInput.R4; end
if isfield(AnalysisInput,'R5'); R5 = AnalysisInput.R5; end
if isfield(AnalysisInput,'RSSI'); RSSI = AnalysisInput.RSSI; end
if isfield(AnalysisInput,'RSSI2'); RSSI2 = AnalysisInput.RSSI2; end
if isfield(AnalysisInput,'RSSI3'); RSSI3 = AnalysisInput.RSSI3; end
if isfield(AnalysisInput,'RSSI4'); RSSI4 = AnalysisInput.RSSI4; end
if isfield(AnalysisInput,'RSSI5'); RSSI5 = AnalysisInput.RSSI5; end
if isfield(AnalysisInput,'StartAngle'); StartAngle = AnalysisInput.StartAngle; end
if isfield(AnalysisInput,'AngleRes'); AngleRes = AnalysisInput.AngleRes; end
if isfield(AnalysisInput,'ApertureAngle'); ApertureAngle = AnalysisInput.ApertureAngle; end
if isfield(AnalysisInput,'SlantAngle'); SlantAngle = AnalysisInput.SlantAngle; end
if isfield(AnalysisInput,'xCoordinateLaser')
    xCoordinateLaser = AnalysisInput.xCoordinateLaser; end
if isfield(AnalysisInput,'aCoordinateLaser')
    aCoordinateLaser = AnalysisInput.aCoordinateLaser; end
if isfield(AnalysisInput,'HeightLaser'); HeightLaser = AnalysisInput.HeightLaser; end
if isfield(AnalysisInput,'SlopeAngleAtLaser')
    SlopeAngleAtLaser = AnalysisInput.SlopeAngleAtLaser; end
if isfield(AnalysisInput,'xCoordinatesMeasured')
    xCoordinatesMeasured = AnalysisInput.xCoordinatesMeasured; end
if isfield(AnalysisInput,'zCoordinatesMeasured')
    zCoordinatesMeasured = AnalysisInput.zCoordinatesMeasured; end
if isfield(AnalysisInput,'aCoordinatesMeasured')
    aCoordinatesMeasured = AnalysisInput.aCoordinatesMeasured; end
if isfield(AnalysisInput,'OrientationAngle')
    OrientationAngle = AnalysisInput.OrientationAngle; end
if isfield(AnalysisInput,'PoleOrientation')
    PoleOrientation = AnalysisInput.PoleOrientation; end
if isfield(AnalysisInput,'aAreaOfInterest')
    aAreaOfInterest = AnalysisInput.aAreaOfInterest; end
if isfield(AnalysisInput,'xAreaOfInterest')
    xAreaOfInterest = AnalysisInput.xAreaOfInterest; end
if isfield(AnalysisInput,'MaxScanDistanceLaser')
    MaxScanDistanceLaser = AnalysisInput.MaxScanDistanceLaser; end

RFinal = R;
if exist('R2','var')==1 && isempty(R2)==0
    %if all echoes were recorded, then use the maximum
    RFinal(R2>RFinal)=R2(R2>RFinal); %distance recorded of all the echoes
    RFinal(R3>RFinal)=R3(R3>RFinal); %to filter out rain, fog, et cetera
    RFinal(R4>RFinal)=R4(R4>RFinal);
    RFinal(R5>RFinal)=R5(R5>RFinal);
end
RFinal = median(RFinal,1,'omitnan');

RSSIFinal = RSSI;
if exist('RSSI2','var')==1 && isempty(RSSI2)==0
    %if all echoes were recorded, then use the maximum
    RSSIFinal(RSSI2>RSSIFinal)=RSSI2(RSSI2>RSSIFinal); %RSSI of all echoes
    RSSIFinal(RSSI3>RSSIFinal)=RSSI3(RSSI3>RSSIFinal); %to filter out rain,
    RSSIFinal(RSSI4>RSSIFinal)=RSSI4(RSSI4>RSSIFinal); %fog, etc.(raindrops
    RSSIFinal(RSSI5>RSSIFinal)=RSSI5(RSSI5>RSSIFinal); %give lowRSSI values)
end
RSSIFinal = median(RSSIFinal,1,'omitnan');

theta1 = StartAngle(1):AngleRes:(ApertureAngle-abs(StartAngle)); %vector of scan angles
theta = theta1-OrientationAngle; %relative scan angles

clear R R2 R3 R4 R5 OrientationAngle StartAngle theta1

%% Determine the CorrectionAngle for the rotation around the y-axis
%CorrectionAngle can lie in the range 0 - 45 deg
CorrectionAngle = 0:0.001:45;

for k=1:1:length(CorrectionAngle)
    %Depending if pole is vertical or perpendicular to slope, different
    %methods are necessary
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %THIS PART FOR A PERPENDICULAR POLE MIGHT NEED TO BE UPDATED AND FIXED%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if strcmp(PoleOrientation,'Perpendicular')==1 %Pole perpendicular to slope
        a = RFinal.*sind(theta); %a=along slope (approximate)
        z1 = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
        z = a.*sind(SlopeAngleAtLaser)+z1.*cosd(SlopeAngleAtLaser);
        %z=vertical (approximate)
        
        id1 = find(min(abs(a-aAreaOfInterest(1)))==abs(a-aAreaOfInterest(1)));
        id2 = find(min(abs(a-aAreaOfInterest(2)))==abs(a-aAreaOfInterest(2)));
        a = a(id1:id2);%only consider area of interest
        z = z(id1:id2);%only consider area of interest
        
        b = diff(a,1,2); %determine differences in a
        bsub = zeros(length(b(:,1)),1);
        bsub(bsub==0)=NaN;
        b = [bsub b]; %add row of NaNs to make length equal to R again
        clear bsub
        c = diff(z,1,2); %determine differences in z
        csub = zeros(length(c(:,1)),1);
        csub(csub==0)=NaN;
        c = [csub c]; %add row of NaNs to make length equal to R again
        clear csub
        Pythagoras = abs(b).^2-abs(c).^2;
        Pythagoras(Pythagoras<0)=NaN;%set negative values to NaN
        Pythagoras = sqrt(Pythagoras);
        %     Pythagoras = hypot(b,c);%determine a-coors using Pythagoras
        clear b c
        Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
        %Step difference cannot be more than difference between laser points
        %at max possible laser scan distance
        Pythagoras(isnan(Pythagoras))=0;%Set NaNs to 0
        x = cumsum(Pythagoras,2);%a-coors are the cumsum of the Pythago steps
        
        for i =1:1:length(a(:,1))
            %find id of laser x-coordinate, at this location, the a-coordinate
            %should also be zero for now (later this is shifted to the laser a-coor
            id(i,1) = find(min(abs(a(i,:)-0))==abs(a(i,:)-0),1);
            x(i,:)=x(i,:)-x(i,id(i));
        end
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    elseif strcmp(PoleOrientation,'Vertical')==1 %Vertical pole
        %For a vertical pole, x and z are exact, a is calculated
        x = RFinal.*sind(theta); %x = horizontal (exact)
        z = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
        % z = vertical (exact)
        
        %only consider the area of interest
        id1 = find(min(abs(x-xAreaOfInterest(1)))==abs(x-xAreaOfInterest(1)));
        id2 = find(min(abs(x-xAreaOfInterest(2)))==abs(x-xAreaOfInterest(2)));
        x = x(id1:id2);
        z = z(id1:id2);
        
        %determine differences in x (increments in x)
        b = diff(x,1,2);
        bsub = zeros(length(b(:,1)),1);
        bsub(bsub==0)=NaN;
        b = [bsub b]; %add row of NaNs to make length equal to R again
        clear bsub
        %determine differences in z (increments in x)
        c = diff(z,1,2); 
        csub = zeros(length(c(:,1)),1);
        csub(csub==0)=NaN;
        c = [csub c]; %add row of NaNs to make length equal to R again
        clear csub
        %determine a-coordinates using Pythagoras (hypotenuse)
        Pythagoras = hypot(b,c);
        clear b c
        Pythagoras(Pythagoras<0)=NaN;%set negative values to NaN
        %the step difference in the a-coordinates cannot be more than the
        %distance between the laser points at the maximum laser scan distance
        Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
        Pythagoras(isnan(Pythagoras))=0;%Set NaNs to 0
        %The a-coordinates are the cumsum of the Pythagoras steps
        a = cumsum(Pythagoras,2);
        
        %Find the location of the laser x-coordinate (= 0). At this location,
        %the a-coordinate should also be zero for now (later this is shifted
        %to the actual laser a-coordinate).
        for i =1:1:length(x(:,1))
            id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
            a(i,:)=a(i,:)-a(i,id(i));
        end
        
    else %error if pole orientation is not supplied
        disp('Error: Supply pole orientation!');
    end
    
    %Rotation matrix to calculate x/z-coordaintes for a rotation around y-axis
    RotationMatrix = [cosd(CorrectionAngle(k)) -sind(CorrectionAngle(k));...
        sind(CorrectionAngle(k)) cosd(CorrectionAngle(k))];
    
    x = x.*RotationMatrix(1,1)+z.*RotationMatrix(1,2);%rotated x (approx)
    z = x.*RotationMatrix(2,1)+z.*RotationMatrix(2,2);%rotated z (approx)
    
    %Determine a-coordinates with Pythagoras again, but now using rotated x and z
    b = diff(x,1,2);
    bsub = zeros(length(b(:,1)),1);
    bsub(bsub==0)=NaN;
    b = [bsub b];
    clear bsub
    c = diff(z,1,2);
    csub = zeros(length(c(:,1)),1);
    csub(csub==0)=NaN;
    c = [csub c];
    clear csub
    Pythagoras = hypot(b,c);
    clear b c
    Pythagoras(Pythagoras<0)=NaN;
    Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
    Pythagoras(isnan(Pythagoras))=0;
    a = cumsum(Pythagoras,2);
    
    %Find the location of the laser x-coordinate (= 0). At this location,
    %the a-coordinate should also be zero for now (later this is shifted
    %to the actual laser a-coordinate).
    for i =1:1:length(x(:,1))
        id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
        a(i,:)=a(i,:)-a(i,id(i));
    end
    
    %Shift the x-coordinates to match the GPS/tape-measurements
    x = xCoordinateLaser + x;
    %Shift the a-coordinates to match the GPS/tape-measurements
    a = aCoordinateLaser + a;
    %Shift the z-coordinates to match the GPS/tape-measurements
    id = find(aCoordinateLaser==aCoordinatesMeasured);
    zLaser = zCoordinatesMeasured(id);
    z = zLaser + z;
    
    %interpolate GPS/tape-measured coordinates for comparison with laser data
    zM = interp1(xCoordinatesMeasured,zCoordinatesMeasured,x);
    
    SquaresError = ((zM-z).^2); %calculate squares
    SquaresError = SquaresError(~isnan(SquaresError)); %remove NaNs
    SumSquaresError(k) = sum(SquaresError); %Calculate sum of local errors
end

MinimumError = min(SumSquaresError); %Calculate least squares (error)
id = find(SumSquaresError==min(SumSquaresError)); %Find id of smallest error
%Find the final correction angle, the angle at which this error is minimum
CorrectionAngle = CorrectionAngle(id);

%% Determine the final corrected cartesian coordinates
%Depending if pole is vertical or perpendicular to slope, different
%methods are necessary

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %THIS PART FOR A PERPENDICULAR POLE MIGHT NEED TO BE UPDATED AND FIXED%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(PoleOrientation,'Perpendicular')==1 %Pole perpendicular to slope
    a = RFinal.*sind(theta); %a=along slope (approximate)
    z1 = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
    z = a.*sind(SlopeAngleAtLaser)+z1.*cosd(SlopeAngleAtLaser);
    %z=vertical (approximate)
    
    id1 = find(min(abs(a-aAreaOfInterest(1)))==abs(a-aAreaOfInterest(1)));
    id2 = find(min(abs(a-aAreaOfInterest(2)))==abs(a-aAreaOfInterest(2)));
    a = a(id1:id2);%only consider area of interest
    z = z(id1:id2);%only consider area of interest
    RSSIFinal = RSSIFinal(id1:id2);%only consider area of interest
    
    b = diff(a,1,2); %determine differences in a
    bsub = zeros(length(b(:,1)),1);
    bsub(bsub==0)=NaN;
    b = [bsub b]; %add row of NaNs to make length equal to R again
    clear bsub
    c = diff(z,1,2); %determine differences in x
    csub = zeros(length(c(:,1)),1);
    csub(csub==0)=NaN;
    c = [csub c]; %add row of NaNs to make length equal to R again
    clear csub
    Pythagoras = abs(b).^2-abs(c).^2;
    Pythagoras(Pythagoras<0)=NaN;%set negative values to NaN
    Pythagoras = sqrt(Pythagoras);
    %     Pythagoras = hypot(b,c);%determine a-coors using Pythagoras
    clear b c
    Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
    %Step difference cannot be more than difference between laser points
    %at max possible laser scan distance
    Pythagoras(isnan(Pythagoras))=0;%Set NaNs to 0
    x = cumsum(Pythagoras,2);%a-coors are the cumsum of the Pythago steps
    
    for i =1:1:length(a(:,1))
        %find id of laser x-coordinate, at this location, the a-coordinate
        %should also be zero for now (later this is shifted to the laser a-coor
        id(i,1) = find(min(abs(a(i,:)-0))==abs(a(i,:)-0),1);
        x(i,:)=x(i,:)-x(i,id(i));
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
elseif strcmp(PoleOrientation,'Vertical')==1 %Vertical pole
    x = RFinal.*sind(theta);
    z = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
    
    id1 = find(min(abs(x-xAreaOfInterest(1)))==abs(x-xAreaOfInterest(1)));
    id2 = find(min(abs(x-xAreaOfInterest(2)))==abs(x-xAreaOfInterest(2)));
    x = x(id1:id2);
    z = z(id1:id2);
    RSSIFinal = RSSIFinal(id1:id2);%now also include the RSSI
    
    b = diff(x,1,2);
    bsub = zeros(length(b(:,1)),1);
    bsub(bsub==0)=NaN;
    b = [bsub b];
    clear bsub
    c = diff(z,1,2);
    csub = zeros(length(c(:,1)),1);
    csub(csub==0)=NaN;
    c = [csub c];
    clear csub
    Pythagoras = hypot(b,c);
    clear b c
    Pythagoras(Pythagoras<0)=NaN;
    Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
    Pythagoras(isnan(Pythagoras))=0;
    a = cumsum(Pythagoras,2);
    
    for i =1:1:length(x(:,1))
        id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
        a(i,:)=a(i,:)-a(i,id(i));
    end
    
else
    disp('Error: Supply pole orientation!');
end

%Rotation matrix to calculate final x- and z-coors for rotation around y-axis
RotationMatrix = [cosd(CorrectionAngle) -sind(CorrectionAngle);...
    sind(CorrectionAngle) cosd(CorrectionAngle)];

x = x.*RotationMatrix(1,1)+z.*RotationMatrix(1,2);%rotated x (approx)
z = x.*RotationMatrix(2,1)+z.*RotationMatrix(2,2);%rotated z (approx)

b = diff(x,1,2);
bsub = zeros(length(b(:,1)),1);
bsub(bsub==0)=NaN;
b = [bsub b];
clear bsub
c = diff(z,1,2);
csub = zeros(length(c(:,1)),1);
csub(csub==0)=NaN;
c = [csub c];
clear csub
Pythagoras = hypot(b,c);
clear b c
Pythagoras(Pythagoras<0)=NaN;
Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
Pythagoras(isnan(Pythagoras))=0;
a = cumsum(Pythagoras,2);

for i =1:1:length(x(:,1))
    id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
    a(i,:)=a(i,:)-a(i,id(i));
end

x = xCoordinateLaser + x;
a = aCoordinateLaser + a;
id = find(aCoordinateLaser==aCoordinatesMeasured);
zLaser = zCoordinatesMeasured(id);
z = zLaser + z;

%Plot a-coordinates and x-coordinates. The a-values should always be larger
%than the x-values!
figure; hold on; grid on; box on;
plot(a); plot(x);
xlabel('id [-]'); ylabel('Coordinate [m]')
legend('a-coordinates','x-coordinates')
title 'The a-coordinates should always be larger than the x-coordinates'

%Prompt, pop-up, window
prompt = ...
    {['Please check the plot.',...
    ' The a-values should always be larger than the x-values.',...
    ' Do the a-coors always lie above the x-coors? (y/n)']};
title1 = 'Check plot';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    disp('Analysis stoppped: check data')
    return
end

%Plot x,z-coordinates of laser versus tape/GPS-measured coordinates
figure; hold on; grid on; box on;
plot(xCoordinatesMeasured,zCoordinatesMeasured);
plot(x,z);
xlabel('x-coor [m]'); ylabel('z-coor [m]')
legend('Measured data','Laser data')
title 'Measured coordinates vs. Laser coordinates'

%pop-up to check the plot
prompt = {'Please check the plot. Do the lines agree well? (y/n)'};
title1 = 'Check plot'; 
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    disp('Analysis stoppped: check data')
    return
end

%% Determine the differences between the measured dike slope and the slope
%  as determined by the laser (deltaSlope)

%interpolate measured coordinates for comparison with laser data
zM = interp1(xCoordinatesMeasured,zCoordinatesMeasured,x);

deltaSlope = abs(zM-z);%difference between measured and laser slopes
id3 = find(deltaSlope==max(deltaSlope));%find id of max difference
deltaSlope = deltaSlope(id3);%determine max difference
loc1 = x(id3);%determine location of max difference

%Plot measured coordinates versus interpolated measured coordinates versus
%laser coordinates, as well as maximum difference between them
figure; hold on; box on; grid on;
plot(xCoordinatesMeasured,zCoordinatesMeasured); %plot measured
plot(x,zM);%plot interpolated measured coordinates for comparison
plot(x,z);%plot laser coordinates
plot(x(id3),z(id3),'r*'); %plot location of max difference
xlabel('x-coor [m]'); ylabel('z-coor [m]');
legend('Measured data','Interpolated measured data','Laser data',...
    'Max difference location')
title({['Max. diff. between laser coors and measured coors: '...
    ,num2str(deltaSlope),...
    ' m, at x = ',num2str(loc1),' m']});

%pop-up to check the plot
prompt = {'Please check the plot. Do the results seem fine? (y/n)'};
title1 = 'Check plot';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    disp('Analysis stoppped: check data')
    return
end

%% Determine output
xDry = x;
aDry = a;
zDry = z;
RSSIDry = RSSIFinal;
AreaOfInterestId1 = id1;
AreaOfInterestId2 = id2;
