% Example of procedure to determine the dry dike slope, to be used in the
% laser scanner analysis
%
% Patrick Oosterlo, 12-2020
%
% Copyright 2020, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Laser 21
% Load data
load('CiaraLaser21DrySlopePart1.mat')
data1 = data;%dry slope data at low tide before the 1st high tide
load('CiaraLaser21DrySlopePart2.mat')
data2 = data;%dry slope data at low tide before the 2nd high tide
load('CiaraLaser21DrySlopePart3.mat')
data3 = data;%dry slope data at low tide before the 3rd high tide
load('CiaraLaser21DrySlopePart4.mat')
data4 = data;%dry slope data at low tide before the 4th high tide
load('CiaraLaser21DrySlopePart5.mat')
data5 = data;%dry slope data at low tide before the 5th high tide

% R, distance
% Use the same filtering method as in the further analysis
R1 = movmedian(data1.R,[2 2],2,'omitnan');%5-point median filter, 1st dimension
R1 = movmedian(R1,[2 2],1,'omitnan');%5-point median filter, 2nd dimension
R1 = median(R1,'omitnan');%determine median of result as being the dry slope
R2 = movmedian(data2.R,[2 2],2,'omitnan');
R2 = movmedian(R2,[2 2],1,'omitnan');
R2 = median(R2,'omitnan');
R3 = movmedian(data3.R,[2 2],2,'omitnan');
R3 = movmedian(R3,[2 2],1,'omitnan');
R3 = median(R3,'omitnan');
R4 = movmedian(data4.R,[2 2],2,'omitnan');
R4 = movmedian(R4,[2 2],1,'omitnan');
R4 = median(R4,'omitnan');
R5 = movmedian(data5.R,[2 2],2,'omitnan');
R5 = movmedian(R5,[2 2],1,'omitnan');
R5 = median(R5,'omitnan');
RTotal = [R1;R2;R3;R4;R5];%Merge the different dry slopes, and determine the
RTotal = median(RTotal,'omitnan');%median once again for the final dry slope

% RSSI, reflectance
% Use the same filtering method as in the further analysis
RSSI1 = movmedian(data1.RSSI,[2 2],2,'omitnan');
RSSI1 = movmedian(RSSI1,[2 2],1,'omitnan');
RSSI1 = median(RSSI1,'omitnan');
RSSI2 = movmedian(data2.RSSI,[2 2],2,'omitnan');
RSSI2 = movmedian(RSSI2,[2 2],1,'omitnan');
RSSI2 = median(RSSI2,'omitnan');
RSSI3 = movmedian(data3.RSSI,[2 2],2,'omitnan');
RSSI3 = movmedian(RSSI3,[2 2],1,'omitnan');
RSSI3 = median(RSSI3,'omitnan');
RSSI4 = movmedian(data4.RSSI,[2 2],2,'omitnan');
RSSI4 = movmedian(RSSI4,[2 2],1,'omitnan');
RSSI4 = median(RSSI4,'omitnan');
RSSI5 = movmedian(data5.RSSI,[2 2],2,'omitnan');
RSSI5 = movmedian(RSSI5,[2 2],1,'omitnan');
RSSI5 = median(RSSI5,'omitnan');
RSSITotal = [RSSI1;RSSI2;RSSI3;RSSI4;RSSI5];
RSSITotal = median(RSSITotal,'omitnan');

% Only necessary for Ciara: find RSSI peaks due to painted grid and remove them
figure; hold on; grid on; box on; plot(RSSITotal);
[~,locs] = findpeaks(RSSITotal,"MinPeakProminence",5);%find peaks
locs = [locs locs+1 locs+2]; %make sure the whole peak is removed
RSSITotal(locs)=NaN;
% fillmissing(RSSITotal,'linear'); %possibility to fill up the missing peaks,
% was not used in the end
plot(RSSITotal);
title 'RSSI peaks due to painted grid'

%Plot the five different dry slopes and the final dry slope for comparison
figure; hold on; grid on; box on;
plot(R1)
plot(R2)
plot(R3)
plot(R4)
plot(R5)
plot(RTotal)
legend('1','2','3','4','5','Total')
title 'Dry slopes, R'

%determine the absolute differences between the dry slopes
a11 = abs(RTotal-R1);
a12 = abs(RTotal-R2);
a13 = abs(RTotal-R3);
a14 = abs(RTotal-R4);
a15 = abs(RTotal-R5);

%determine the maximum differences at the actual slope,
%not at the shallow flat in front of the dike or over the crest (here 100:540)
R1diff = max(a11(100:540))
R2diff = max(a12(100:540))
R3diff = max(a13(100:540))
R4diff = max(a14(100:540))
R5diff = max(a15(100:540))

%plot the differences for comparison
figure; hold on; grid on; box on;
plot(a11)
plot(a12)
plot(a13)
plot(a14)
plot(a15)
legend('1-1','1-2','1-3','1-4','1-5')
title 'Dry slope differences, R'

%Plot the five different dry slopes and the final dry slope for comparison
figure; hold on;
grid on; box on;
plot(RSSI1)
plot(RSSI2)
plot(RSSI3)
plot(RSSI4)
plot(RSSI5)
plot(RSSITotal)
legend('1','2','3','4','5','Total')
title 'Dry slopes, RSSI'

%determine the absolute differences between the dry slopes
b11 = abs(RSSITotal-RSSI1);
b12 = abs(RSSITotal-RSSI2);
b13 = abs(RSSITotal-RSSI3);
b14 = abs(RSSITotal-RSSI4);
b15 = abs(RSSITotal-RSSI5);

%determine the maximum differences at the actual slope,
%not at the shallow flat in front of the dike or over the crest (here 100:540)
max(b11(100:540))
max(b12(100:540))
max(b13(100:540))
max(b14(100:540))
max(b15(100:540))

%plot the differences for comparison
figure; hold on; grid on; box on;
plot(b11)
plot(b12)
plot(b13)
plot(b14)
plot(b15)
legend('1-1','1-2','1-3','1-4','1-5')
title 'Dry slope differences, RSSI'

%Collect data, in this case for the last echo only
data = [];
data.R = RTotal;
data.R2 = [];
data.R3 = [];
data.R4 = [];
data.R5 = [];
data.RSSI = RSSITotal;
data.RSSI2 = [];
data.RSSI3 = [];
data.RSSI4 = [];
data.RSSI5 = [];
data.StartAngle = data5.StartAngle(1);
data.AngleRes = data5.AngleRes;

%Show data saving prompt/window
prompt = {'Save data to *.mat file? (y/n):'};
title1 = 'Save data?';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    [filename, filepath] = uiputfile('*.mat', 'Save the file:');
    FileName = fullfile(filepath, filename);
    save(FileName, 'data', '-v7.3');
end

% Only for Ciara: remove gridlines in actual measured data with waves
% Load file with data during high tide (storm peak)
load('CiaraPeak4Laser21.mat')
% Remove RSSI-data at grid lines
data.RSSI(:,locs) = NaN;
% Save data
save('CiaraPeak4Laser21newRemovedGridLines.mat','data','-v7.3')

%% Laser 27
% Load data
load('CiaraLaser27DrySlopePart1.mat')
data1 = data;%dry slope data at low tide before the 1st high tide
load('CiaraLaser27DrySlopePart2.mat')
data2 = data;%dry slope data at low tide before the 2nd high tide
load('CiaraLaser27DrySlopePart3.mat')
data3 = data;%dry slope data at low tide before the 3rd high tide
load('CiaraLaser27DrySlopePart4.mat')
data4 = data;%dry slope data at low tide before the 4th high tide
load('CiaraLaser27DrySlopePart5.mat')
data5 = data;%dry slope data at low tide before the 5th high tide

% R, distance
% Use the same filtering method as in the further analysis
R1 = movmedian(data1.R,[2 2],2,'omitnan');%5-point median filter, 1st dimension
R1 = movmedian(R1,[2 2],1,'omitnan');%5-point median filter, 2nd dimension
R1 = median(R1,'omitnan');%determine median of result as being the dry slope
R2 = movmedian(data2.R,[2 2],2,'omitnan');
R2 = movmedian(R2,[2 2],1,'omitnan');
R2 = median(R2,'omitnan');
R3 = movmedian(data3.R,[2 2],2,'omitnan');
R3 = movmedian(R3,[2 2],1,'omitnan');
R3 = median(R3,'omitnan');
R4 = movmedian(data4.R,[2 2],2,'omitnan');
R4 = movmedian(R4,[2 2],1,'omitnan');
R4 = median(R4,'omitnan');
R5 = movmedian(data5.R,[2 2],2,'omitnan');
R5 = movmedian(R5,[2 2],1,'omitnan');
R5 = median(R5,'omitnan');
RTotal = [R1;R2;R3;R4;R5];%Merge the different dry slopes, and determine the
RTotal = median(RTotal,'omitnan');%median once again for the final dry slope

% RSSI, reflectance
% Use the same filtering method as in the further analysis
RSSI1 = movmedian(data1.RSSI,[2 2],2,'omitnan');
RSSI1 = movmedian(RSSI1,[2 2],1,'omitnan');
RSSI1 = median(RSSI1,'omitnan');
RSSI2 = movmedian(data2.RSSI,[2 2],2,'omitnan');
RSSI2 = movmedian(RSSI2,[2 2],1,'omitnan');
RSSI2 = median(RSSI2,'omitnan');
RSSI3 = movmedian(data3.RSSI,[2 2],2,'omitnan');
RSSI3 = movmedian(RSSI3,[2 2],1,'omitnan');
RSSI3 = median(RSSI3,'omitnan');
RSSI4 = movmedian(data4.RSSI,[2 2],2,'omitnan');
RSSI4 = movmedian(RSSI4,[2 2],1,'omitnan');
RSSI4 = median(RSSI4,'omitnan');
RSSI5 = movmedian(data5.RSSI,[2 2],2,'omitnan');
RSSI5 = movmedian(RSSI5,[2 2],1,'omitnan');
RSSI5 = median(RSSI5,'omitnan');
RSSITotal = [RSSI1;RSSI2;RSSI3;RSSI4;RSSI5];
RSSITotal = median(RSSITotal,'omitnan');

% Only necessary for Ciara: find RSSI peaks due to painted grid and remove them
figure; hold on; grid on; box on; plot(RSSITotal);
[~,locs] = findpeaks(RSSITotal,"MinPeakProminence",5);%find peaks
locs = [locs locs+1 locs+2]; %make sure the whole peak is removed
RSSITotal(locs)=NaN;
% fillmissing(RSSITotal,'linear'); %possibility to fill up the missing peaks,
% was not used in the end
plot(RSSITotal);
title 'RSSI peaks due to painted grid'

%Plot the five different dry slopes and the final dry slope for comparison
figure; hold on; grid on; box on;
plot(R1)
plot(R2)
plot(R3)
plot(R4)
plot(R5)
plot(RTotal)
legend('1','2','3','4','5','Total')
title 'Dry slopes, R'

%determine the absolute differences between the dry slopes
a11 = abs(RTotal-R1);
a12 = abs(RTotal-R2);
a13 = abs(RTotal-R3);
a14 = abs(RTotal-R4);
a15 = abs(RTotal-R5);

%determine the maximum differences at the actual slope,
%not at the shallow flat in front of the dike or over the crest (here 100:540)
R1diff = max(a11(100:540))
R2diff = max(a12(100:540))
R3diff = max(a13(100:540))
R4diff = max(a14(100:540))
R5diff = max(a15(100:540))

%plot the differences for comparison
figure; hold on; grid on; box on;
plot(a11)
plot(a12)
plot(a13)
plot(a14)
plot(a15)
legend('1-1','1-2','1-3','1-4','1-5')
title 'Dry slope differences, R'

%Plot the five different dry slopes and the final dry slope for comparison
figure; hold on;
grid on; box on;
plot(RSSI1)
plot(RSSI2)
plot(RSSI3)
plot(RSSI4)
plot(RSSI5)
plot(RSSITotal)
legend('1','2','3','4','5','Total')
title 'Dry slopes, RSSI'

%determine the absolute differences between the dry slopes
b11 = abs(RSSITotal-RSSI1);
b12 = abs(RSSITotal-RSSI2);
b13 = abs(RSSITotal-RSSI3);
b14 = abs(RSSITotal-RSSI4);
b15 = abs(RSSITotal-RSSI5);

%determine the maximum differences at the actual slope,
%not at the shallow flat in front of the dike or over the crest (here 100:540)
max(b11(100:540))
max(b12(100:540))
max(b13(100:540))
max(b14(100:540))
max(b15(100:540))

%plot the differences for comparison
figure; hold on; grid on; box on;
plot(b11)
plot(b12)
plot(b13)
plot(b14)
plot(b15)
legend('1-1','1-2','1-3','1-4','1-5')
title 'Dry slope differences, RSSI'

%Collect data, in this case for the last echo only
data = [];
data.R = RTotal;
data.R2 = [];
data.R3 = [];
data.R4 = [];
data.R5 = [];
data.RSSI = RSSITotal;
data.RSSI2 = [];
data.RSSI3 = [];
data.RSSI4 = [];
data.RSSI5 = [];
data.StartAngle = data5.StartAngle(1);
data.AngleRes = data5.AngleRes;

%Show data saving prompt/window
prompt = {'Save data to *.mat file? (y/n):'};
title1 = 'Save data?';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    [filename, filepath] = uiputfile('*.mat', 'Save the file:');
    FileName = fullfile(filepath, filename);
    save(FileName, 'data', '-v7.3');
end

% Only for Ciara: remove gridlines in actual measured data with waves
% Load file with data during high tide (storm peak)
load('CiaraPeak4Laser27.mat')
% Remove RSSI-data at grid lines
data.RSSI(:,locs) = NaN;
% Save data
save('CiaraPeak4Laser27newRemovedGridLines.mat','data','-v7.3')
