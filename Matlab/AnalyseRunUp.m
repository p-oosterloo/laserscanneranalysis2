function RunUpResults = AnalyseRunUp(AnalysisInput)
% RunUpResults = AnalyseRunUp(AnalysisInput)
%
% read in data from laser scanner *.mat file containing waves running up
% the slope, and determine the layer thickness and run-up
%
% a-coordinate is defined as the distance along the slope
% x-coordinate is defined as the horizontal distance
% z-coordinate is defined as the height (vertically upward)
%
% input:
% AnalysisInput data structure, with fields:
%   .R [R2,R3,R4,R5]         = radial distance from scanner (m) [for all echoes]
%   .RSSI                    = RSSI data (reflected signal intensity) (-)
%   .StartAngle              = start angle (deg.)
%   .AngleRes                = angle resolution (deg.)
%   .ApertureAngle           = aperture angle of laser scanner (deg.)
%   .SlantAngle              = slant angle of scanner (deg.)
%   .xCoordinateLaser        = x-coordinate (horizontal) of scanner (m)
%   .aCoordinateLaser        = a-coordinate (along slope) of scanner (m)
%   .HeightLaser             = height of scanner above slope (m)
%   .SlopeAngleAtLaser       = angle of dike slope at scanner (deg.)
%   .xCoordinatesMeasured    = measured x-coordinates (horizontal)
%                              (e.g. with measurement tape) (m)
%   .zCoordinatesMeasured    = measured z-coordinates (vertical)
%                              (e.g. with measurement tape) (m)
%   .aCoordinatesMeasured    = measured a-coordinates (along slope)
%                              (e.g. with measurement tape) (m)
%   .OrientationAngle        = angle at which scanner scans straight down
%                              (theta=0) (deg.)
%   .PoleOrientation         = orientation of laser pole, 'Vertical' or
%                              'Perpendicular'
%   .CorrectionAngle         = correction angle for rotation around y-axis (deg)
%   .MaxScanDistanceLaser    = maximum distance that laser can scan (m)
%   .AreaOfInterestId1       = ID that indicates where the area of interest starts;
%   .AreaOfInterestId2       = ID that indicates where the area of interest ends;
%   .T                       = test time series (s);
%   .DateAndHour             = date and hour time series (datetime format)
%   .xDry                    = x-coordinates (horizontal) of dry slope (m)
%   .aDry                    = a-coordinates (along slope) of dry slope (m)
%   .zDry                    = z-coordinates (vertical) of dry slope (m)
%   .RSSIDry                 = RSSI values of dry slope (-)
%   .TimePeriod              = Requested time period for analysis, in cell
%                              with strings and in American format 
%                              {'mm-dd-yyyy hh:mm:ss' 'mm-dd-yyyy hh:mm:ss'}
%   .LayerThicknessThreshold = layer thickness threshold for analysis (m)
%   .NaNMaxLT                = Number of straight NaNs after which no valid
%                              layer thickness may occur, to be used in layer
%                              thickness analysis (-) (e.g. 10 or 20)
%   .RSSIThreshold           = RSSI difference threshold for analysis (-)
%   .NaNMaxRSSI              = Number of straight NaNs after which no valid
%                              RSSI value may occur, to be used in analysis
%                              (-) (e.g. 10 or 20)
%   .WaterLevel              = water level during measurement (constant) (m)
%   .WavePeriod              = mean wave period (or 0.8*peak period) (s)
%
% output:
% RunUpResults data structure, with fields:
%   .MinPercentageValid     = minimum percentage of valid measurements at
%                            certain location (%)
%   .PercentageFiltered1cm  = percentage of points that changed more than 1
%                            cm due to filtering (%)
%   .PercentageFiltered5cm  = percentage of points that changed more than 5
%                            cm due to filtering (%)
%   .ErosionDepth           = increase in erosion depth from start to end of
%                            test (m)
%   .LayerThicknessThreshold= layer thickness threshold for analysis (m)
%   .RSSIThreshold          = RSSI difference threshold for analysis (-)
%   .NaNMaxLT               = Number of straight NaNs after which no valid
%                             layer thickness may occur, to be used in layer
%                             thickness analysis (-) (e.g. 10 or 20)
%   .NaNMaxRSSI             = Number of straight NaNs after which no valid
%                             RSSI value may occur, to be used in analysis
%                             (-) (e.g. 10 or 20)
%   .NumberOfWavesFoundx    = Number of waves found based on x run-up analysis (-)
%   .NumberOfWavesFounda    = Number of waves found based on a run-up analysis (-)
%   .NumberOfWavesFoundz    = Number of waves found based on z run-up analysis (-)
%   .NumberOfWavesFoundRSSIx= Number of waves found based on x RSSI analysis (-)
%   .NumberOfWavesFoundRSSIa= Number of waves found based on a RSSI analysis (-)
%   .NumberOfWavesFoundRSSIz= Number of waves found based on z RSSI analysis (-)
%   .WaterLevel             = water level during measurement (constant) (m)
%   .WavePeriod             = mean wave period (or 0.8*peak period) (s)
%   .T                      = test time series (s);
%   .DateAndHour            = date and hour time series (datetime format)
%   .LayerThickness         = Timeseries of layer thickness (m);
%   .aDry                   = a-coordinates of dry slope (m)
%   .xDry                   = x-coordinates of dry slope (m)
%   .zDry                   = z-coordinates of dry slope (m)
%   .RSSIDry                = RSSI-values of dry slope (m)
%   .xAbsolute              = layer thickness x-coors time series (absolute) (m)
%   .aAbsolute              = layer thickness a-coors time series (absolute) (m)
%   .zAbsolute              = layer thickness z-coors time series (absolute) (m)
%   .xRSSIAbsolute          = layer thickness RSSI x-coors time series (absolute) (m)
%   .aRSSIAbsolute          = layer thickness RSSI a-coors time series (absolute) (m)
%   .zRSSIAbsolute          = layer thickness RSSI z-coors time series (absolute) (m)
%   .zRelative              = layer thickness z-coors time series (relative) (m)
%   .zRSSIRelative          = layer thickness RSSI z-coors time series (relative) (m)
%   .xRunUpAbsolute         = Run-up series in x-coordinates based on layer
%                             thickness (absolute) (m)
%   .aRunUpAbsolute         = Run-up series in a-coordinates based on layer
%                             thickness (absolute) (m)
%   .zRunUpAbsolute         = Run-up series in z-coordinates based on layer
%                             thickness (absolute) (m)
%   .xRunUpRSSIAbsolute     = Run-up series in x-coordinates based on RSSI
%                             (absolute) (m)
%   .aRunUpRSSIAbsolute     = Run-up series in a-coordinates based on RSSI
%                             (absolute) (m)
%   .zRunUpRSSIAbsolute     = Run-up series in z-coordinates based on RSSI
%                             (absolute) (m)
%   .zRunUpRelative         = Run-up series in z-coordinates based on layer
%                             thickness (relative) (m)
%   .zRunUpRSSIRelative     = Run-up series in z-coordinates based on RSSI
%                             (relative) (m)
%   .xRunUpLocs             = IDs of x run-up peaks based on layer thickness (-)
%   .aRunUpLocs             = IDs of a run-up peaks based on layer thickness (-)
%   .zRunUpLocs             = IDs of z run-up peaks based on layer thickness (-)
%   .xRunUpRSSILocs         = IDs of x run-up peaks based on RSSI (-)
%   .aRunUpRSSILocs         = IDs of a run-up peaks based on RSSI (-)
%   .zRunUpRSSILocs         = IDs of z run-up peaks based on RSSI (-)
%   .TxRunUpPeaks           = Times of x run-up peaks based on layer
%                             thickness (s)
%   .TaRunUpPeaks           = Times of a run-up peaks based on layer
%                             thickness (s)
%   .TzRunUpPeaks           = Times of z run-up peaks based on layer
%                             thickness (s)
%   .TxRunUpRSSIPeaks       = Times of x run-up peaks based on RSSI (s)
%   .TaRunUpRSSIPeaks       = Times of a run-up peaks based on RSSI (s)
%   .TzRunUpRSSIPeaks       = Times of z run-up peaks based on RSSI (s)
%   .DateAndHourxRunUpPeaks = Dates and hours of x run-up peaks based on layer
%                             thickness (s)
%   .DateAndHouraRunUpPeaks = Dates and hours of a run-up peaks based on layer
%                             thickness (s)
%   .DateAndHourzRunUpPeaks = Dates and hours of z run-up peaks based on layer
%                             thickness (s)
%   .DateAndHourxRunUpRSSIPeaks = Dates and hours of x run-up peaks based on RSSI (s)
%   .DateAndHouraRunUpRSSIPeaks = Dates and hours of a run-up peaks based on RSSI (s)
%   .DateAndHourzRunUpRSSIPeaks = Dates and hours of z run-up peaks based on RSSI (s)
%   .xRunUpPeaksAbsolute    = Run-up peaks in x-coordinates based on layer
%                             thickness (absolute) (m)
%   .aRunUpPeaksAbsolute    = Run-up peaks in a-coordinates based on layer
%                             thickness (absolute) (m)
%   .zRunUpPeaksAbsolute    = Run-up peaks in z-coordinates based on layer
%                             thickness (absolute) (m)
%   .xRunUpRSSIPeaksAbsolute = Run-up peaks in x-coordinates based on RSSI
%                              (absolute) (m)
%   .aRunUpRSSIPeaksAbsolute = Run-up peaks in a-coordinates based on RSSI
%                              (absolute) (m)
%   .zRunUpRSSIPeaksAbsolute = Run-up peaks in z-coordinates based on RSSI
%                              (absolute) (m)
%   .zRunUpPeaksRelative     = Run-up peaks in z-coordinates based on RSSI
%                              (relative) (m)
%   .zRunUpRSSIPeaksRelative = Run-up peaks in z-coordinates based on RSSI
%                              (relative) (m)
%   .Ru2PctAbsolute         = Ru2%-value based on measured distance R
%                             (absolute) (m)
%   .Ru2PctRelative         = Ru2%-value based on measured distance R
%                             (relative) (m)
%   .Ru2PctRSSIAbsolute     = Ru2%-value based on RSSI (absolute) (m)
%   .Ru2PctRSSIRelative     = Ru2%-value based on RSSI (relative) (m)
% Figure with run-up timeseries and found run-up peaks for both layer
%   thickness and RSSI analyses
%
% Patrick Oosterlo, 12-2020
%
% Copyright 2020, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Load input
if isfield(AnalysisInput,'R'); R = AnalysisInput.R; end
if isfield(AnalysisInput,'R2'); R2 = AnalysisInput.R2; end
if isfield(AnalysisInput,'R3'); R3 = AnalysisInput.R3; end
if isfield(AnalysisInput,'R4'); R4 = AnalysisInput.R4; end
if isfield(AnalysisInput,'R5'); R5 = AnalysisInput.R5; end
if isfield(AnalysisInput,'RSSI'); RSSI = AnalysisInput.RSSI; end
if isfield(AnalysisInput,'RSSI2'); RSSI2 = AnalysisInput.RSSI2; end
if isfield(AnalysisInput,'RSSI3'); RSSI3 = AnalysisInput.RSSI3; end
if isfield(AnalysisInput,'RSSI4'); RSSI4 = AnalysisInput.RSSI4; end
if isfield(AnalysisInput,'RSSI5'); RSSI5 = AnalysisInput.RSSI5; end
if isfield(AnalysisInput,'StartAngle'); StartAngle = AnalysisInput.StartAngle; end
if isfield(AnalysisInput,'AngleRes'); AngleRes = AnalysisInput.AngleRes; end
if isfield(AnalysisInput,'ApertureAngle'); ApertureAngle = AnalysisInput.ApertureAngle; end
if isfield(AnalysisInput,'SlantAngle'); SlantAngle = AnalysisInput.SlantAngle; end
if isfield(AnalysisInput,'xCoordinateLaser')
    xCoordinateLaser = AnalysisInput.xCoordinateLaser; end
if isfield(AnalysisInput,'aCoordinateLaser')
    aCoordinateLaser = AnalysisInput.aCoordinateLaser; end
if isfield(AnalysisInput,'HeightLaser'); HeightLaser = AnalysisInput.HeightLaser; end
if isfield(AnalysisInput,'SlopeAngleAtLaser')
    SlopeAngleAtLaser = AnalysisInput.SlopeAngleAtLaser; end
if isfield(AnalysisInput,'xCoordinatesMeasured')
    xCoordinatesMeasured = AnalysisInput.xCoordinatesMeasured; end
if isfield(AnalysisInput,'zCoordinatesMeasured')
    zCoordinatesMeasured = AnalysisInput.zCoordinatesMeasured; end
if isfield(AnalysisInput,'aCoordinatesMeasured')
    aCoordinatesMeasured = AnalysisInput.aCoordinatesMeasured; end
if isfield(AnalysisInput,'OrientationAngle')
    OrientationAngle = AnalysisInput.OrientationAngle; end
if isfield(AnalysisInput,'PoleOrientation')
    PoleOrientation = AnalysisInput.PoleOrientation; end
if isfield(AnalysisInput,'CorrectionAngle')
    CorrectionAngle = AnalysisInput.CorrectionAngle; end
if isfield(AnalysisInput,'MaxScanDistanceLaser')
    MaxScanDistanceLaser = AnalysisInput.MaxScanDistanceLaser; end
if isfield(AnalysisInput,'AreaOfInterestId1')
    AreaOfInterestId1 = AnalysisInput.AreaOfInterestId1; end
if isfield(AnalysisInput,'AreaOfInterestId2')
    AreaOfInterestId2 = AnalysisInput.AreaOfInterestId2; end
if isfield(AnalysisInput,'T'); T = AnalysisInput.T; end
if isfield(AnalysisInput,'DateAndHour')
    DateAndHour = AnalysisInput.DateAndHour; end
if isfield(AnalysisInput,'xDry'); xDry = AnalysisInput.xDry; end
if isfield(AnalysisInput,'aDry'); aDry = AnalysisInput.aDry; end
if isfield(AnalysisInput,'zDry'); zDry = AnalysisInput.zDry; end
if isfield(AnalysisInput,'RSSIDry'); RSSIDry = AnalysisInput.RSSIDry; end
if isfield(AnalysisInput,'TimePeriod')
    TimePeriod = AnalysisInput.TimePeriod; end
if isfield(AnalysisInput,'LayerThicknessThreshold')
    LayerThicknessThreshold = AnalysisInput.LayerThicknessThreshold; end
if isfield(AnalysisInput,'RSSIThreshold')
    RSSIThreshold = AnalysisInput.RSSIThreshold; end
if isfield(AnalysisInput,'NaNMaxLT')
    NaNMaxLT = AnalysisInput.NaNMaxLT; end
if isfield(AnalysisInput,'NaNMaxRSSI')
    NaNMaxRSSI = AnalysisInput.NaNMaxRSSI; end
if isfield(AnalysisInput,'WaterLevel')
    WaterLevel = AnalysisInput.WaterLevel; end
if isfield(AnalysisInput,'WavePeriod')
    WavePeriod = AnalysisInput.WavePeriod; end

%Find ids corresponding to requested time period
TimePeriodId1 = ...
    find(min(abs(datenum(DateAndHour)-datenum(TimePeriod{1})))==...
    abs(datenum(DateAndHour)-datenum(TimePeriod{1})));
TimePeriodId2 = ...
    find(min(abs(datenum(DateAndHour)-datenum(TimePeriod{2})))==...
    abs(datenum(DateAndHour)-datenum(TimePeriod{2})));

RFinal = R;
if exist('R2','var')==1 %if all echoes were recorded, then use the maximum
    RFinal(R2>RFinal)=R2(R2>RFinal); %distance recorded of all the echoes
    RFinal(R3>RFinal)=R3(R3>RFinal); %to filter out rain, fog, et cetera
    RFinal(R4>RFinal)=R4(R4>RFinal);
    RFinal(R5>RFinal)=R5(R5>RFinal);
end
RFinal = RFinal(...
    TimePeriodId1:TimePeriodId2,AreaOfInterestId1:AreaOfInterestId2);

RSSIFinal = RSSI;
if exist('R2','var')==1 %if all echoes were recorded, then use the maximum
    RSSIFinal(RSSI2>RSSIFinal)=RSSI2(RSSI2>RSSIFinal); %RSSI of all echoes
    RSSIFinal(RSSI3>RSSIFinal)=RSSI3(RSSI3>RSSIFinal); %to filter out rain,
    RSSIFinal(RSSI4>RSSIFinal)=RSSI4(RSSI4>RSSIFinal); %fog, etc.(raindrops
    RSSIFinal(RSSI5>RSSIFinal)=RSSI5(RSSI5>RSSIFinal); %give lowRSSI values)
end
RSSIFinal = RSSIFinal(...
    TimePeriodId1:TimePeriodId2,AreaOfInterestId1:AreaOfInterestId2);

T = T(TimePeriodId1:TimePeriodId2); %cut to only use requested time period
DateAndHour = DateAndHour(TimePeriodId1:TimePeriodId2);
StartAngle = StartAngle(TimePeriodId1:TimePeriodId2);

theta1 = StartAngle(1):AngleRes:(ApertureAngle-abs(StartAngle)); %vector of scan angles
theta = theta1-OrientationAngle; %relative scan angles
theta = theta(AreaOfInterestId1:AreaOfInterestId2);

clear R R2 R3 R4 R5 RSSI RSSI2 RSSI3 RSSI4 RSSI5 OrientationAngle ...
    StartAngle theta1 AreaOfInterestId1 AreaOfInterestId2 TimePeriodId1 ...
    TimePeriodId2

%% Determine the final corrected cartesian coordinates
%Depending if pole is vertical or perpendicular to slope, different
%methods are necessary

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %THIS PART FOR A PERPENDICULAR POLE MIGHT NEED TO BE UPDATED AND FIXED%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(PoleOrientation,'Perpendicular')==1 %Pole perpendicular to slope
    a = RFinal.*sind(theta); %a=along slope (approximate)
    z1 = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
    z = a.*sind(SlopeAngleAtLaser)+z1.*cosd(SlopeAngleAtLaser);
    %z=vertical (approximate)
    
    b = diff(a,1,2); %determine differences in a
    bsub = zeros(length(b(:,1)),1);
    bsub(bsub==0)=NaN;
    b = [bsub b]; %add row of NaNs to make length equal to R again
    clear bsub
    c = diff(z,1,2); %determine differences in z
    csub = zeros(length(c(:,1)),1);
    csub(csub==0)=NaN;
    c = [csub c]; %add row of NaNs to make length equal to R again
    clear csub
    Pythagoras = abs(b).^2-abs(c).^2;
    Pythagoras(Pythagoras<0)=NaN;%set negative values to NaN
    Pythagoras = sqrt(Pythagoras);
    %     Pythagoras = hypot(b,c);%determine a-coors using Pythagoras
    clear b c
    Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
    %Step difference cannot be more than difference between laser points
    %at max possible laser scan distance
    Pythagoras(isnan(Pythagoras))=0;%Set NaNs to 0
    x = cumsum(Pythagoras,2);%a-coors are the cumsum of the Pythago steps
    
    for i =1:1:length(a(:,1))
        %find id of laser x-coordinate, at this location, the a-coordinate
        %should also be zero for now (later this is shifted to the laser a-coor
        id(i,1) = find(min(abs(a(i,:)-0))==abs(a(i,:)-0),1);
        x(i,:)=x(i,:)-x(i,id(i));
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
elseif strcmp(PoleOrientation,'Vertical')==1 %Vertical pole
    x = RFinal.*sind(theta);
    z = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
    
    b = diff(x,1,2);
    bsub = zeros(length(b(:,1)),1);
    bsub(bsub==0)=NaN;
    b = [bsub b];
    clear bsub
    c = diff(z,1,2);
    csub = zeros(length(c(:,1)),1);
    csub(csub==0)=NaN;
    c = [csub c];
    clear csub
    Pythagoras = hypot(b,c);
    clear b c
    Pythagoras(Pythagoras<0)=NaN;
    Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
    Pythagoras(isnan(Pythagoras))=0;
    a = cumsum(Pythagoras,2);
    
    for i =1:1:length(x(:,1))
        id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
        a(i,:)=a(i,:)-a(i,id(i));
    end
    
else
    disp('Error: Supply pole orientation!');
end

%Rotation matrix to calculate final x- and z-coors for rotation around y-axis
RotationMatrix = [cosd(CorrectionAngle) -sind(CorrectionAngle);...
    sind(CorrectionAngle) cosd(CorrectionAngle)];

x = x.*RotationMatrix(1,1)+z.*RotationMatrix(1,2);%rotated x (approx)
z = x.*RotationMatrix(2,1)+z.*RotationMatrix(2,2);%rotated z (approx)

b = diff(x,1,2);
bsub = zeros(length(b(:,1)),1);
bsub(bsub==0)=NaN;
b = [bsub b];
clear bsub
c = diff(z,1,2);
csub = zeros(length(c(:,1)),1);
csub(csub==0)=NaN;
c = [csub c];
clear csub
Pythagoras = hypot(b,c);
clear b c
Pythagoras(Pythagoras<0)=NaN;
Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
Pythagoras(isnan(Pythagoras))=0;
a = cumsum(Pythagoras,2);

for i =1:1:length(x(:,1))
    id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
    a(i,:)=a(i,:)-a(i,id(i));
end

x = xCoordinateLaser + x;
a = aCoordinateLaser + a;
id = find(aCoordinateLaser==aCoordinatesMeasured);
zLaser = zCoordinatesMeasured(id);
z = zLaser + z;

clear Pythagoras RFinal

%% Determine run-up
%determine the percentage of valid measurements at each location
%(so the percentage that is not a NaN)
PercentageValid = sum(~isnan(z),1)./length(z(:,1))*100;
%find the minimum of these percentages
minPercentageValid = min(PercentageValid);

%plot this percentage as function of z-coordinate
figure('units','normalized','outerposition',[0 0 0.5 1]);
hold on; grid on; box on;
plot(x(1,:),PercentageValid,'k.')
xlabel('x-coordinate [m]');
ylabel('Percentage of valid measurements [%]')
legend('Percentage of valid measurements [%]')
title(['Minimum percentage of valid measurements over whole slope = ',...
    num2str(min(PercentageValid)),' %'])

%pop-up to check the plot
prompt = ...
    {'Please check the plot. Is the minimum percentage sufficiently high? (y/n)'};
title1 = 'Check plot';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%5-point median filter in space and in time, excluding NaNs
aDryFilter = movmedian(aDry, [2 2], 2, 'omitnan');
xDryFilter = movmedian(xDry, [2 2], 2, 'omitnan');
zDryFilter = movmedian(zDry, [2 2], 2, 'omitnan');
RSSIDryFilter = movmedian(RSSIDry, [2 2], 2, 'omitnan');
aFilter = movmedian(a, [2 2], 2, 'omitnan');
xFilter = movmedian(x, [2 2], 2, 'omitnan');
zFilter = movmedian(z, [2 2], 2, 'omitnan');
RSSIFilter = movmedian(RSSIFinal, [2 2], 2, 'omitnan');

aDryFilter = movmedian(aDryFilter, [2 2], 1, 'omitnan');
xDryFilter = movmedian(xDryFilter, [2 2], 1, 'omitnan');
zDryFilter = movmedian(zDryFilter, [2 2], 1, 'omitnan');
RSSIDryFilter = movmedian(RSSIDryFilter, [2 2], 1, 'omitnan');
aFilter = movmedian(aFilter, [2 2], 1, 'omitnan');
xFilter = movmedian(xFilter, [2 2], 1, 'omitnan');
zFilter = movmedian(zFilter, [2 2], 1, 'omitnan');
RSSIFilter = movmedian(RSSIFilter, [2 2], 1, 'omitnan');

%find the percentage of points that changed due to the filter
%changed more than 1 cm:
PercentageFiltered1cm = ...
    sum(sum(abs(zFilter-z)>0.01))/(length(z(:,1))*length(z(1,:)))*100;
%changed more than 5 cm:
PercentageFiltered5cm = ...
    sum(sum(abs(zFilter-z)>0.05))/(length(z(:,1))*length(z(1,:)))*100;
clear aDry xDry zDry RSSIDry a x z RSSIFinal %clear some stuff

%interpolate in space, with constant spacing towards densest laserdata spacing
InterpResolution = HeightLaser*tand(AngleRes);%resolution
xVector = min(xFilter(:)):InterpResolution:max(xFilter(:));%new vector length
%interpolation crashes if data is not unique, so add a tiny value
aDryUnique = aDryFilter + linspace(0, 1, length(aDryFilter(1,:)))*1E-10;
xDryUnique = xDryFilter + linspace(0, 1, length(xDryFilter(1,:)))*1E-10;
zDryUnique = zDryFilter + linspace(0, 1, length(zDryFilter(1,:)))*1E-10;
RSSIDryUnique = RSSIDryFilter + linspace(0, 1, length(RSSIDryFilter(1,:)))*1E-10;
aUnique = aFilter + linspace(0, 1, length(aFilter(1,:)))*1E-10;
xUnique = xFilter + linspace(0, 1, length(xFilter(1,:)))*1E-10;
zUnique = zFilter + linspace(0, 1, length(zFilter(1,:)))*1E-10;
RSSIUnique = RSSIFilter + linspace(0, 1, length(RSSIFilter(1,:)))*1E-10;
clear aDryFilter xDryFilter zDryFilter RSSIDryFilter%clear some stuff
clear aFilter xFilter zFilter RSSIFilter%clear some stuff
%interpolation does not work with NaNs, so fill remaining NaN values linearly
aDryUnique = fillmissing(aDryUnique,'linear');
xDryUnique = fillmissing(xDryUnique,'linear');
zDryUnique = fillmissing(zDryUnique,'linear');
RSSIDryUnique = fillmissing(RSSIDryUnique,'linear');
aUnique = fillmissing(aUnique,'linear');
xUnique = fillmissing(xUnique,'linear');
zUnique = fillmissing(zUnique,'linear');
RSSIUnique = fillmissing(RSSIUnique,'linear');

%help vectors for interpolation of xUnique to xInterp
a = linspace(0,1,length(xDryUnique(1,:)));
b = linspace(0,1,length(xVector(1,:)));

%Everything needs to be interpolated towards a-coordinates, because the
%layer thickness has to be determined at points with the same a-coordinates
%Interpolate to densest laser spacing first
aDryInterp = interp1(a,aDryUnique,b);%interpolate aDry
xDryInterp = interp1(aDryUnique,xDryUnique,aDryInterp);%interpolate xDry
zDryInterp = interp1(aDryUnique,zDryUnique,aDryInterp);%interpolate zDry
RSSIDryInterp = interp1(aDryUnique,RSSIDryUnique,aDryInterp);%interpolate RSSIDry
clear xDryUnique aDryUnique zDryUnique RSSIDryUnique %clear some stuff

aInterp = interp1(a,aUnique',b);%interpolate a
aInterp = aInterp';
clear a b %clear some stuff

xInterp = zeros(size(aInterp));%create empty matrix
for i = 1:1:length(xUnique(:,1))%interpolate x
    xInterp(i,:) = interp1(aUnique(i,:),xUnique(i,:),aInterp(i,:));
end
clear xUnique %clear some stuff
zInterp = zeros(size(aInterp));%create empty matrix
for i = 1:1:length(zUnique(:,1))%interpolate z
    zInterp(i,:) = interp1(aUnique(i,:),zUnique(i,:),aInterp(i,:));
end
clear zUnique %clear some stuff
RSSIInterp = zeros(size(aInterp));%create empty matrix
for i = 1:1:length(RSSIUnique(:,1))%interpolate RSSI
    RSSIInterp(i,:) = interp1(aUnique(i,:),RSSIUnique(i,:),aInterp(i,:));
end
clear RSSIUnique aUnique xVector %clear some stuff

%Then, interpolate to a-coordinates of dry slope, so layer thickness can be
%determined correctly
for i = 1:1:length(xInterp(:,1))%interpolate x
    xInterp(i,:) = interp1(aInterp(i,:),xInterp(i,:),aDryInterp);%,'linear','extrap');
end
for i = 1:1:length(zInterp(:,1))%interpolate z
    zInterp(i,:) = interp1(aInterp(i,:),zInterp(i,:),aDryInterp);%,'linear','extrap');
end
for i = 1:1:length(RSSIInterp(:,1))%interpolate RSSI
    RSSIInterp(i,:) = interp1(aInterp(i,:),RSSIInterp(i,:),aDryInterp);%,'linear','extrap');
end
for i = 1:1:length(aInterp(:,1))%interpolate a
    aInterp(i,:) = aDryInterp;
end

% %Remove unrealistically small or large values due to extrapolation
% xInterp(xInterp>1000 | xInterp<0)=NaN;
% zInterp(zInterp>1000 | zInterp<0)=NaN;
% RSSIInterp(RSSIInterp>255 | RSSIInterp<0)=NaN; %maximum RSSI=255

%find remaining NaNs and determine percentages
NumberOfNaNs = sum(isnan(zInterp));
PercentageOfNaNs = NumberOfNaNs./length(zInterp(:,1)).*100;
xDryInterp(:,PercentageOfNaNs>99)=[]; %remove column if it contains
aDryInterp(:,PercentageOfNaNs>99)=[]; %more than 99% NaN values
zDryInterp(:,PercentageOfNaNs>99)=[];
RSSIDryInterp(:,PercentageOfNaNs>99)=[];
xInterp(:,PercentageOfNaNs>99)=[]; %remove column if it contains
aInterp(:,PercentageOfNaNs>99)=[]; %more than 99% NaN values
zInterp(:,PercentageOfNaNs>99)=[];
RSSIInterp(:,PercentageOfNaNs>99)=[];

figure('units','normalized','outerposition',[0 0 0.5 1]);
hold on; grid on; box on; %plot interpolated data
plot(xCoordinatesMeasured,zCoordinatesMeasured); %plot measured
plot(xInterp(100,:),zInterp(100,:));% plot interpolated
xlabel('x-coordinate [m]'); ylabel('z-coordinate [m]');
legend('Measured data','Interpolated laser data');
title(['Laser coordinates versus measured coordinates \newline',...
    'Laser data may have a wave on the slope'])

prompt = {'Please check the plot. Do the lines seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

MedianScanFrequency = median(1./diff(T)); %determine median scan frequency

%Determine maximum erosion depth along slope
%Note: this is the vertical depth (z-coordinates)!
ErosionDepths = zInterp-zDryInterp;
ErosionDepth = min(ErosionDepths).*-1;
[~,peakLocs] = findpeaks(ErosionDepth,'MinPeakHeight',0.01,...
    'MinPeakDistance',10,'NPeaks',4,'SortStr','descend'); %find object locations
peakLocs = sort(peakLocs);

figure; hold on; box on; grid on; %plot possible object locations
plot(xDryInterp,ErosionDepth);
plot(xDryInterp(peakLocs),ErosionDepth(peakLocs),'r*')
xlabel('x-coor [m]'); ylabel('Maximum erosion [m]')
title(['Maximum (vertical) erosion depth along slope [m]',...
    '\newline Note that errors may occur due to wave troughs'])

prompt = {[num2str(length(peakLocs)),...
    ' erosion peaks found. Please enter the most likely (and maximum)',...
    ' erosion location number. If no locations were found, enter: 0']};
title1 = 'Check plot'; %pop-up to mark the correct object location
dims = [1 40];
definput = {'1'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'1')
    ErosionDepth=ErosionDepth(peakLocs(1));
    close gcf;
elseif strcmp(answer,'2')
    ErosionDepth=ErosionDepth(peakLocs(2));
    close gcf;
elseif strcmp(answer,'3')
    ErosionDepth=ErosionDepth(peakLocs(3));
    close gcf;
elseif strcmp(answer,'4')
    ErosionDepth=ErosionDepth(peakLocs(4));
    close gcf;
elseif strcmp(answer,'0')
    ErosionDepth=[];
end

[a,b] = find(ErosionDepth==ErosionDepths.*-1,1,'last'); %find erosion row number
ErosionID = [a b]; clear a b ErosionDepths

% %Determine run-up difference based on z-coordinates
% RunUpDifference = zInterp-zDryInterp;
%Determine actual layer thickness
%Wave on the slope: zWet>zDry and xWet<xDry!
LayerThickness = sqrt(sign(zInterp-zDryInterp).*(zInterp-zDryInterp).^2+...
    sign(xDryInterp-xInterp).*(xDryInterp-xInterp).^2);
LayerThickness = real(LayerThickness);
%Determine RSSI difference, assuming that RSSIWet>RSSIDry
RSSIDifference = RSSIInterp-RSSIDryInterp;
%Use the x and z coordinates to use as a reference system with the RSSI
xRSSIInterp = xInterp;
aRSSIInterp = aInterp;
zRSSIInterp = zInterp;

% Measured Distance R
%Extra data filters for e.g. rain or people
% RunUpDifference(RunUpDifference<0)=0; %RunUpDifference<0 =0
% RunUpDifference(RunUpDifference>1.5)=NaN; %RunUpDifference>1.5=NaN (rain/person)
LayerThickness(LayerThickness<=0)=NaN; %LayerThickness<=0 =NaN
LayerThickness(LayerThickness>1.5)=NaN; %LayerThickness>1.5=NaN (rain/person)

%Big local difference (>0.5 m) in LayerThickness in 1 time step = rain
b = abs(diff(LayerThickness,1,1));
bsub = zeros(1,length(b(1,:)));
bsub(bsub==0)=NaN;
b = [bsub; b]; %add row of NaNs to make length equal to R again
LayerThickness(b>0.5)=NaN;
clear bsub b

%Big local difference (>0.5 m) in LayerThickness in 1 space step = rain
b = abs(diff(LayerThickness,1,2));
bsub = zeros(length(b(:,1)),1);
bsub(bsub==0)=NaN;
b = [bsub b]; %add row of NaNs to make length equal to R again
LayerThickness(b>0.5)=NaN;
clear bsub b

%Use layer thickness threshold to remove outliers
% RunUpDifference(RunUpDifference<LayerThicknessThreshold)=NaN;
LayerThickness(LayerThickness<LayerThicknessThreshold)=NaN;

%A valid value again after more than NaNMax straight NaNs (in space),
%means noise or rain or something standing in the laserbeam
for i = 1:1:length(LayerThickness(:,1))
    b = strfind(isnan(LayerThickness(i,:)),true(1,NaNMaxLT)); %find x straight nans
    if length(b)>=1
        for k = b(1):length(LayerThickness(1,:))
            if LayerThickness(i,k)>0 %find layer thickness >0
                LayerThickness(i,k)=NaN; %set to NaN
            end
        end
    end
end
clear b

% LayerThickness(isnan(RunUpDifference))=NaN;

% Laser reflection intensity RSSI
%Big local difference in RSSI (>100) in 1 time step = rain
b = abs(diff(RSSIDifference,1,1));
bsub = zeros(1,length(b(1,:)));
bsub(bsub==0)=NaN;
b = [bsub; b]; %add row of NaNs to make length equal to R again
RSSIDifference(b>100)=NaN;
clear bsub b

%Big local difference in RSSI (>100) in 1 space step = rain
b = abs(diff(RSSIDifference,1,2));
bsub = zeros(length(b(:,1)),1);
bsub(bsub==0)=NaN;
b = [bsub b]; %add row of NaNs to make length equal to R again
RSSIDifference(b>100)=NaN;
clear bsub b

%Use RSSI threshold to remove outliers
RSSIDifference(RSSIDifference<RSSIThreshold)=NaN;

%A valid value again after more than NaNMax straight NaNs (in space),
%means noise or rain or something standing in the laserbeam
for i = 1:1:length(RSSIDifference(:,1))
    b = strfind(isnan(RSSIDifference(i,:)),true(1,NaNMaxRSSI));%find x straight NaNs
    if length(b)>=1
        RSSIDifference(i,b(1):end)=NaN;%set to NaN
    end
end

%Plot dry slope and eroded slope
figure('units','normalized','outerposition',[0 0 0.5 1]);
hold on; grid on; box on;
plot(xDryInterp,zDryInterp);
if isempty(ErosionID)==1
else
    plot(xInterp(ErosionID(1),:),zInterp(ErosionID(1),:),'k-');
    plot(xInterp(ErosionID(1),ErosionID(2)),zInterp(ErosionID(1),ErosionID(2)),'r*');
end
xlabel('x-coordinate [m]'); ylabel('z-coordinate [m]');
legend('Dry slope laser','Wave run-up data laser');
if isempty(ErosionID)==1
    title(['Growth of erosion during test = 0 m',...
            '\newline Note that errors may occur due to wave troughs'])
else
    title(['Growth of erosion during test = ',...
        num2str(ErosionDepth),' m',...
            '\newline Note that errors may occur due to wave troughs'])
end

prompt = {'Please check the plots. Do the results seem fine? (y/n)'};
title1 = 'Check plots'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check data')
    return
end

%plot layer thickness to check irregularities
[X,Y] = meshgrid(T(1:50:end),xInterp(1,:));
figure('units','normalized','outerposition',[0 0 0.5 1]);
surf(X,Y,LayerThickness(1:50:end,:)')
xlabel('Time [s]'); ylabel('x-coordinate [m]');
zlabel('Layer thickness [m]')
title(['Layer thickness and run-up in time',...
    '\newline Make sure there are no spurious run-ups'])
view(215,45)

prompt = {'Please check the plot. Do the data look fine, and are there no spurious run-ups? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: re-assess the data')
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%There are two methods to determine the layer thickness and RSSI difference
%1: difference between dry slope at the beginning and wet slope during test
%2: difference between dry slope during test and wet slope during test
%Method 1 seems to work better for the layer thickness
%Combination of methods 1&2 seems to work better for the RSSI difference
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Distance: Method 1:
%filter below layer thickness threshold
xInterp(LayerThickness<LayerThicknessThreshold)=NaN;
aInterp(LayerThickness<LayerThicknessThreshold)=NaN;
zInterp(LayerThickness<LayerThicknessThreshold)=NaN;
%if layer thickness=NaN, then waves=NaN
xInterp(isnan(LayerThickness))=NaN;
aInterp(isnan(LayerThickness))=NaN;
zInterp(isnan(LayerThickness))=NaN;

%Distance: Method 2
% %filter for difference in RSSI smaller than x
% zWaves2 = zWaves;
% zRunUp2  =zeros(length(zInterp(:,1)),1);
% for j = 1:length(zInterp(:,1))-5
%     for i=1:1:length(zInterp(1,:))
%         %verschil(j,i) = abs(RSSI(j,i)-RSSI(j+1,i));
%         if abs(zInterp(j,i)-zInterp(j+250,i))<0.01
%             zWaves2(j,i) = nan;
%         end
%     end
%     zRunUp2(j) = max(zWaves2(j,:));
% end

%Determine run-up based on measured distance R
%determine x- and a-run-ups in time
[xRunUp,~] = max(xInterp,[],2);
[aRunUp,~] = max(aInterp,[],2);
%determine z-run-ups in time
%sometimes a valid value occurs after a NaN,
%so better to find the last value that is not a NaN
zRunUp = zeros(size(xRunUp));
for i = 1:1:length(zInterp)
    if isempty(find(~isnan(zInterp(i,:)),1,'last'))==1
        zRunUp(i) = NaN;
    else
        zRunUp(i) = zInterp(i,find(~isnan(zInterp(i,:)),1,'last'));
    end
end

%RSSI: Method 1:
%filter below RSSI threshold
xRSSIInterp(RSSIDifference<RSSIThreshold)=NaN;
aRSSIInterp(RSSIDifference<RSSIThreshold)=NaN;
zRSSIInterp(RSSIDifference<RSSIThreshold)=NaN;
%if layer thickness=NaN, then waves=NaN
xRSSIInterp(isnan(RSSIDifference))=NaN;
aRSSIInterp(isnan(RSSIDifference))=NaN;
zRSSIInterp(isnan(RSSIDifference))=NaN;

%RSSI: Method 2:
%filter for difference >RSSIThreshold
%remove data point if difference between value at time t and value at time
%t+5 is larger than RSSIThreshold
for j = 1:length(RSSIInterp(:,1))-5
    for i=1:1:length(RSSIInterp(1,:))
        if abs(RSSIInterp(j,i)-RSSIInterp(j+5,i))<RSSIThreshold
            RSSIDifference(j,i) = NaN;
            xRSSIInterp(j,i) = NaN;
            aRSSIInterp(j,i) = NaN;
            zRSSIInterp(j,i) = NaN;
        end
    end
end

%Determine run-up based on RSSI
%determine x- and a-run-ups in time
[xRunUpRSSI,~] = max(xRSSIInterp,[],2);
[aRunUpRSSI,~] = max(aRSSIInterp,[],2);
%sometimes a valid value occurs after a NaN,
%so better to find the last value that is not a NaN
zRunUpRSSI = zeros(size(xRunUpRSSI));
for i = 1:1:length(zRSSIInterp)
    if isempty(find(~isnan(zRSSIInterp(i,:)),1,'last'))==1
        zRunUpRSSI(i) = NaN;
    else
        zRunUpRSSI(i) = zRSSIInterp(i,find(~isnan(zRSSIInterp(i,:)),1,'last'));
    end
end

%Smoothen run-up signals
%small window median filter to remove some of the noise, but keep the peaks
xRunUp = movmedian(xRunUp,[2 2], 1, 'omitnan');
% xRunUp = movmedian(xRunUp,[2 2], 2, 'omitnan');
aRunUp = movmedian(aRunUp,[2 2], 1, 'omitnan');
% aRunUp = movmedian(aRunUp,[2 2], 2, 'omitnan');
zRunUp = movmedian(zRunUp,[2 2], 1, 'omitnan');
% zRunUp = movmedian(zRunUp,[2 2], 2, 'omitnan');
zRunUpRSSI = movmedian(zRunUpRSSI,[2 2], 1, 'omitnan');
% zRunUpRSSI = movmedian(zRunUpRSSI,[2 2], 2, 'omitnan');

%Find maximum run-up per wave, with a minimum run-up of 10 cm and a
%minimum peak distance of 0.75*mean wave period
%find id of z-coordinates where z-coordinate is equal to water level and
%use this id to find x-coor, a-coor, RSSI-coor of min. run-up
idMinRunUp = find(min(abs(WaterLevel+0.1-zDryInterp))...
    ==abs(WaterLevel+0.1-zDryInterp));
[xPeaks,xLocs] = findpeaks(xRunUp,'MinPeakHeight',xDryInterp(idMinRunUp),...
    'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);
[aPeaks,aLocs] = findpeaks(aRunUp,'MinPeakHeight',aDryInterp(idMinRunUp),...
    'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);
[zPeaks,zLocs] = findpeaks(zRunUp,'MinPeakHeight',zDryInterp(idMinRunUp),...
    'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);
%The same for the RSSI
[xPeaksRSSI,xLocsRSSI] = findpeaks(xRunUpRSSI,'MinPeakHeight',xDryInterp(idMinRunUp),...
    'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);
[aPeaksRSSI,aLocsRSSI] = findpeaks(aRunUpRSSI,'MinPeakHeight',aDryInterp(idMinRunUp),...
    'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);
[zPeaksRSSI,zLocsRSSI] = findpeaks(zRunUpRSSI,'MinPeakHeight',zDryInterp(idMinRunUp),...
    'MinPeakDistance',0.75*WavePeriod*MedianScanFrequency);

%Plot the different run-up results
figure('units','normalized','outerposition',[0 0 0.5 1]);
hold on; grid on; box on;
plot(DateAndHour,xRunUp);
plot(DateAndHour,xRunUpRSSI);
plot(DateAndHour(xLocs),xPeaks,'bo')
plot(DateAndHour(xLocsRSSI),xPeaksRSSI,'ro')
xlabel 'time [s]'; ylabel 'x-level [m]';
legend('x run-up (R)','x run-up (RSSI)','x run-up peaks (R)','x run-up peaks (RSSI)')
title(['x Run-up peaks, number of peaks: ',num2str(length(xPeaks)),...
    ' (R-distance), ',num2str(length(xPeaksRSSI)),' (RSSI)']);

figure('units','normalized','outerposition',[0 0 0.5 1]);
hold on; grid on; box on;
plot(DateAndHour,aRunUp);
plot(DateAndHour,aRunUpRSSI);
plot(DateAndHour(aLocs),aPeaks,'bo')
plot(DateAndHour(aLocsRSSI),aPeaksRSSI,'ro')
xlabel 'time [s]'; ylabel 'a-level [m]';
legend('a run-up (R)','a run-up (RSSI)','a run-up peaks (R)','a run-up peaks (RSSI)')
title(['a Run-up peaks, number of peaks: ',num2str(length(aPeaks)),...
    ' (R-distance), ',num2str(length(aPeaksRSSI)),' (RSSI)']);

figure('units','normalized','outerposition',[0 0 0.5 1]);
hold on; grid on; box on;
plot(DateAndHour,zRunUp);
plot(DateAndHour,zRunUpRSSI);
plot(DateAndHour(zLocs),zPeaks,'bo')
plot(DateAndHour(zLocsRSSI),zPeaksRSSI,'ro')
xlabel 'time [s]'; ylabel 'z-level [m]';
legend('z run-up (R)','z run-up (RSSI)','z run-up peaks (R)','z run-up peaks (RSSI)')
title(['z Run-up peaks, number of peaks: ',num2str(length(zPeaks)),...
    ' (R-distance), ',num2str(length(zPeaksRSSI)),' (RSSI)']);

prompt = {'Please check the plots. Do the signals and peaks seem fine? (y/n)'};
title1 = 'Check plots'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    display('Analysis stoppped: check minimum numer of NaNs, layer thickness or RSSI threshold')
    return
end

%Determine relative (to water level) run-up, so the actual run-up as
%defined in e.g. EurOtop (2018)
zRelative = zInterp - WaterLevel;
zRSSIRelative = zRSSIInterp - WaterLevel;
zRunUpRelative = zRunUp - WaterLevel;
zRunUpRSSIRelative = zRunUpRSSI - WaterLevel;
zPeaksRelative = zPeaks - WaterLevel;
zPeaksRSSIRelative = zPeaksRSSI - WaterLevel;

%Determine 2% run-up (absolute and relative)
Ru2PctId = round(length(zPeaks)/100*98);
SortedPks = sort(zPeaks);
Ru2Pct = SortedPks(Ru2PctId);
Ru2PctRelative = Ru2Pct - WaterLevel;
% display(['2% Run-up = ',num2str(Ru2PctRelative),' m']);

Ru2PctId = round(length(zPeaksRSSI)/100*98);
SortedPks = sort(zPeaksRSSI);
Ru2PctRSSI = SortedPks(Ru2PctId);
Ru2PctRSSIRelative = Ru2PctRSSI - WaterLevel;
% display(['2% Run-up = ',num2str(Ru2PctRSSIRelative),' m']);

%Plot relative run-up
RunUpResultsFigure = figure('units','normalized','outerposition',[0 0 0.5 1]);
hold on; grid on; box on;
plot(DateAndHour,zRunUpRelative);
plot(DateAndHour,zRunUpRSSIRelative);
plot(DateAndHour(zLocs),zPeaksRelative,'bo')
plot(DateAndHour(zLocsRSSI),zPeaksRSSIRelative,'ro')
xlabel 'time [s]'; ylabel 'z-level [m]';
legend('z run-up (R)','z run-up (RSSI)','z run-up peaks (R)','z run-up peaks (RSSI)')
title(['z Run-up peaks, R_{u2}%: ',num2str(Ru2PctRelative),...
    ' (R-distance), ',num2str(Ru2PctRSSIRelative),' (RSSI)']);

prompt = {'Please check the plots. Do the signals and peaks seem fine? (y/n)'};
title1 = 'Check plots'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'n')
    display('Analysis stoppped: check minimum numer of NaNs, layer thickness or RSSI threshold')
    return
end

%% Generate and save output
%Combine all output into a RunUpResults structure
RunUpResults.minPercentageValid = minPercentageValid;
RunUpResults.PercentageFiltered1cm = PercentageFiltered1cm;
RunUpResults.PercentageFiltered5cm = PercentageFiltered5cm;
RunUpResults.ErosionDepth = ErosionDepth;
RunUpResults.LayerThicknessThreshold = LayerThicknessThreshold;
RunUpResults.RSSIThreshold = RSSIThreshold;
RunUpResults.NaNMaxLT = NaNMaxLT;
RunUpResults.NaNMaxRSSI = NaNMaxRSSI;
RunUpResults.NumberOfWavesFoundx = length(xPeaks);
RunUpResults.NumberOfWavesFounda = length(aPeaks);
RunUpResults.NumberOfWavesFoundz = length(zPeaks);
RunUpResults.NumberOfWavesFoundRSSIx = length(xPeaksRSSI);
RunUpResults.NumberOfWavesFoundRSSIa = length(aPeaksRSSI);
RunUpResults.NumberOfWavesFoundRSSIz = length(zPeaksRSSI);
RunUpResults.WaterLevel = WaterLevel;
RunUpResults.WavePeriod = WavePeriod;
RunUpResults.T = T;
RunUpResults.DateAndHour = DateAndHour;
RunUpResults.LayerThickness = LayerThickness;
RunUpResults.xDry = xDryInterp;
RunUpResults.aDry = aDryInterp;
RunUpResults.zDry = zDryInterp;
RunUpResults.RSSIDry = RSSIDryInterp;
RunUpResults.xAbsolute = xInterp;
RunUpResults.aAbsolute = aInterp;
RunUpResults.zAbsolute = zInterp;
RunUpResults.xRSSIAbsolute = xRSSIInterp;
RunUpResults.aRSSIAbsolute = aRSSIInterp;
RunUpResults.zRSSIAbsolute = zRSSIInterp;
RunUpResults.zRelative = zRelative;
RunUpResults.zRSSIRelative = zRSSIRelative;
RunUpResults.xRunUpAbsolute = xRunUp;
RunUpResults.aRunUpAbsolute = aRunUp;
RunUpResults.zRunUpAbsolute = zRunUp;
RunUpResults.xRunUpRSSIAbsolute = xRunUpRSSI;
RunUpResults.aRunUpRSSIAbsolute = aRunUpRSSI;
RunUpResults.zRunUpRSSIAbsolute = zRunUpRSSI;
RunUpResults.zRunUpRelative = zRunUpRelative;
RunUpResults.zRunUpRSSIRelative = zRunUpRSSIRelative;
RunUpResults.xRunUpLocs = xLocs;
RunUpResults.aRunUpLocs = aLocs;
RunUpResults.zRunUpLocs = zLocs;
RunUpResults.xRunUpRSSILocs = xLocsRSSI;
RunUpResults.aRunUpRSSILocs = aLocsRSSI;
RunUpResults.zRunUpRSSILocs = zLocsRSSI;
RunUpResults.TxRunUpPeaks = T(xLocs);
RunUpResults.TaRunUpPeaks = T(aLocs);
RunUpResults.TzRunUpPeaks = T(zLocs);
RunUpResults.TxRunUpRSSIPeaks = T(xLocsRSSI);
RunUpResults.TaRunUpRSSIPeaks = T(aLocsRSSI);
RunUpResults.TzRunUpRSSIPeaks = T(zLocsRSSI);
RunUpResults.DateAndHourxRunUpPeaks = DateAndHour(xLocs);
RunUpResults.DateAndHouraRunUpPeaks = DateAndHour(aLocs);
RunUpResults.DateAndHourzRunUpPeaks = DateAndHour(zLocs);
RunUpResults.DateAndHourxRunUpRSSIPeaks = DateAndHour(xLocsRSSI);
RunUpResults.DateAndHouraRunUpRSSIPeaks = DateAndHour(aLocsRSSI);
RunUpResults.DateAndHourzRunUpRSSIPeaks = DateAndHour(zLocsRSSI);
RunUpResults.xRunUpPeaksAbsolute = xPeaks;
RunUpResults.aRunUpPeaksAbsolute = aPeaks;
RunUpResults.zRunUpPeaksAbsolute = zPeaks;
RunUpResults.xRunUpRSSIPeaksAbsolute = xPeaksRSSI;
RunUpResults.aRunUpRSSIPeaksAbsolute = aPeaksRSSI;
RunUpResults.zRunUpRSSIPeaksAbsolute = zPeaksRSSI;
RunUpResults.zRunUpPeaksRelative = zPeaksRelative;
RunUpResults.zRunUpRSSIPeaksRelative = zPeaksRSSIRelative;
RunUpResults.Ru2PctAbsolute = Ru2Pct;
RunUpResults.Ru2PctRelative = Ru2PctRelative;
RunUpResults.Ru2PctRSSIAbsolute = Ru2PctRSSI;
RunUpResults.Ru2PctRSSIRelative = Ru2PctRSSIRelative;

%Prompt to save data and run-up figure
prompt = {'Save data? (y/n):'};
title1 = 'Save data?';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    [filename1, filepath1] = uiputfile('*.mat', 'Save the run-up file:');
    FileName = fullfile(filepath1, filename1);
    save(FileName, 'RunUpResults', '-v7.3');
    [filename, pathname] = uiputfile('*.fig','Save figure as',...
        'RunUpResultsFigure');
    if isnumeric(filename)
        disp('User pushed cancel. Not saving anything')
    else
        savefig(RunUpResultsFigure,fullfile(pathname, filename))
    end
end
