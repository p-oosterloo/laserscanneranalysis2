function OrientationAngle = DetermineOrientationAngle(AnalysisInput)
% function OrientationAngle = DetermineOrientationAngle(AnalysisInput);
%
% Read in data from laser scanner *.mat file, with an object placed on the
% slope directly under the laser scanner. Determine the orientation angle
% of the laser: the scan angle at which the laser scans straight down (theta=0)
%
% Note: the *.mat file needs to contain a stationary and uniform situation,
% so no temporal or spatial changes in the slope or object may occur!
%
% input:
% AnalysisInput data structure, with fields:
%   .R(time,angle)      = radial distance from scanner (m)
%   .StartAngle         = start angle (deg.)
%   .AngleRes           = angle resolution (deg.)
%   .ApertureAngle      = aperture angle of laser scanner (deg.)
%
% output:
%   OrientationAngle    = the orientation angle (deg.)
%
% Patrick Oosterlo, 12-2020
%
% Copyright 2020, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Load input
if isfield(AnalysisInput,'R'); R = AnalysisInput.R; end
if isfield(AnalysisInput,'StartAngle'); StartAngle = AnalysisInput.StartAngle; end
if isfield(AnalysisInput,'AngleRes'); AngleRes = AnalysisInput.AngleRes; end
if isfield(AnalysisInput,'ApertureAngle'); ApertureAngle = AnalysisInput.ApertureAngle; end

%% Determine orientation angle
theta1 = StartAngle(1):AngleRes:(ApertureAngle-abs(StartAngle)); %vector of scan angles

RFinal = median(R,1,'omitnan'); %median of measured distances
dRdR = diff(diff(RFinal));%2nd derivative >0 gives a local minimum in R
dRdR = [NaN,NaN,dRdR];%make sure length is equal to R again
[~,peakLocs] = findpeaks(dRdR,'MinPeakHeight',0.02,...
    'MinPeakDistance',100,'NPeaks',4,'SortStr','descend'); %find object locations
peakLocs = sort(peakLocs);

%Matlab often gives the locations just to the right of the peak instead of
%at the peak, so this is fixed here
peakLocs = peakLocs-1; 

figure; hold on; box on; grid on; %plot potential object locations
plot(RFinal);
plot(peakLocs,RFinal(peakLocs),'r*')
xlabel('id no [-]'); ylabel('R-distance [m]')
title 'Potential object locations'

% Prompt to mark correct object location
prompt = {[num2str(length(peakLocs)),...
    ' locations found. Please enter the right object location number.',...
    ' If no locations were found, enter: 0']};
title1 = 'Check plot'; %pop-up to mark the correct object location
dims = [1 40];
definput = {'1'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'1')
    a=peakLocs(1);
    close gcf;
elseif strcmp(answer,'2')
    a=peakLocs(2);
    close gcf;
elseif strcmp(answer,'3')
    a=peakLocs(3);
    close gcf;
elseif strcmp(answer,'4')
    a=peakLocs(4);
    close gcf;
elseif strcmp(answer,'0')
    disp('Analysis stoppped: check data')
    return
end

OrientationAngle = theta1(a); %find angle at which the laser scans exactly downward