function AngleOfIncidenceResults = AnalyseAngleOfIncidence(AnalysisInput)
% AngleOfIncidenceResults = AnalyseAngleOfIncidence(AnalysisInput)
%
% read in data based on 1 or 2 FrontVelocitiesAndOvertoppingResults
% (front velocity and overtopping analysis) structures (1 or 2 lasers) and
% determine the wave (run-up) peak period based on 1 or 2 laser run-up
% signals.
% The angle of incidence can only be determined based on 2 laser run-up
% signals. The delay between the signals, the cross-spectral density and
% linear wave theory are used for this.
% If only one laser signal is supplied, only the wave (run-up) peak periods
% are determined, not the angles of incidence, as two laser signals are
% necessary for this.
%
% a-coordinate is defined as the distance along the slope
% x-coordinate is defined as the horizontal distance
% z-coordinate is defined as the height (vertically upward)
%
% input:
% AnalysisInput data structure, with fields:
%   .T                        = test time series (s);
%   .DateAndHour              = date and hour time series (datetime format)
%   .zRunUpRelativeSmooth1    = smoothed run-up timeseries in relative
%                               z-coordinates (laser 1)
%   .zRunUpRelativeSmooth2    = smoothed run-up timeseries in relative
%                               z-coordinates (laser 2)
%                               This command is optional, without it,
%                               no angle of indicence is calculated,
%                               only the wave (run-up) peak period
%   .WaterLevel               = water level during measurement (constant) (m)
%   .BottomLevel              = bottom level at toe of dike (m) or (m+NAP)
%   .DistanceBetweenScanLines = distance between laser scanner scan lines (m)
%
% output:
% AngleOfIncidenceResults data structure, with fields:
%   .T                        = test time series (s);
%   .DateAndHour              = date and hour time series (datetime format)
%   .zRunUpRelativeSmooth1    = smoothed run-up timeseries in relative
%                               z-coordinates (laser 1)
%   .zRunUpRelativeSmooth2    = smoothed run-up timeseries in relative
%                               z-coordinates (laser 2)
%                               This command is optional, without it,
%                               no angle of indicence is calculated,
%                               only the wave (run-up) peak period
%   .WaterLevel               = water level during measurement (constant) (m)
%   .BottomLevel              = bottom level at toe of dike (m) or (m+NAP)
%   .DistanceBetweenScanLines = distance between laser scanner scan lines (m)
%   .SpectralFrequencies1     = Energy density spectral frequencies of
%                               laser 1 (Hz)
%   .SpectralDensities1       = Energy density of laser 1 (m2/Hz/deg)
%   .SpectralFrequencies2     = Energy density spectral frequencies of
%                               laser 2 (Hz)
%   .SpectralDensities2       = Energy density of laser 2 (m2/Hz/deg)
%   .PeakFrequencies1         = Spectral peak frequencies of laser 1,
%                               in descending order (Hz)
%   .PeakPeriods1             = Spectral peak periods of laser 1,
%                               in descending order (s)
%   .PeakFrequencies2         = Spectral peak frequencies of laser 2,
%                               in descending order (Hz)
%   .PeakPeriods2             = Spectral peak periods of laser 2,
%                               in descending order (s)
%   .CrossSpectralFrequencies = Range of frequencies of cross-spectral
%                               density (Hz)
%   .CrossSpectralDensity     = Cross-spectral density (m2/Hz/deg)
%   .AngleOfIncidenceSpectrum = Angle of incidence spectrum (deg)
%   .AngleOfIncidencePeaks1   = Angle of incidence at spectral peaks of 
%                               laser 1 (deg)
%   .AngleOfIncidencePeaks2   = Angle of incidence at spectral peaks of 
%                               laser 2 (deg)
%   .AngleOfIncidenceMean     = Mean angle of incidence, range 0-1 Hz (deg)
%   .AngleOfIncidenceMean     = Median angle of incidence, range 0-1 Hz (deg)
%   .AngleOfIncidenceMean     = Standard deviation of angle of incidence,
%                               range 0-1 Hz (deg)
%
% Patrick Oosterlo, 01-2021
%
% Copyright 2021, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Load input
if isfield(AnalysisInput,'T')
    T = AnalysisInput.T; end
if isfield(AnalysisInput,'DateAndHour')
    DateAndHour = AnalysisInput.DateAndHour; end
if isfield(AnalysisInput,'zRunUpRelativeSmooth1')
    LS1 = AnalysisInput.zRunUpRelativeSmooth1; end
if isfield(AnalysisInput,'zRunUpRelativeSmooth2')
    LS2 = AnalysisInput.zRunUpRelativeSmooth2; end
if isfield(AnalysisInput,'WaterLevel')
    WaterLevel = AnalysisInput.WaterLevel; end
if isfield(AnalysisInput,'BottomLevel')
    BottomLevel = AnalysisInput.BottomLevel; end
if isfield(AnalysisInput,'DistanceBetweenScanLines')
    DistanceBetweenScanLines = AnalysisInput.DistanceBetweenScanLines; end

%% Determine wave (run-up) peak period
WaterDepth = WaterLevel - BottomLevel;
MedianScanFrequency = median(diff(T)); %determine median scan frequency

if exist('LS2')==0
    LS2 = [];
end

if isempty(LS2)==1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Extra smoothing is not necessary if signals are loaded from        %
    %FrontVelocitiesAndOvertoppingResults structure                     %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % LS1 = movmedian(LS1, [5 5],1,'omitnan');%11 pts
    % LS1 = movmean(LS1, [10 10],1,'omitnan');%21 pts
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Make sure run-up data is sufficiently smooth!!!
    %Plot run-up signal(s)
    figure; hold on; grid on; box on;
    plot(T,LS1);
    xlabel 'time [s]'; ylabel 'a-level [m]';
    title 'Run-up signal(s)'
    legend('Laser 1','Laser 2')
    
    prompt = {'Please check the plots. Do the signals seem fine? (y/n)'};
    title1 = 'Check plots'; %pop-up to check the plot
    dims = [1 35];
    definput = {'y'};
    opts.WindowStyle = 'Normal';
    answer = inputdlg(prompt,title1,dims,definput,opts);
    if strcmp(answer,'y')
        close gcf;
    elseif strcmp(answer,'n')
        display('Analysis stoppped. Check the smoothness of the signals')
        return
    end
    
    %Determine spectra
    [F1,Pxx1] = spectrumsimple(MedianScanFrequency,LS1,'fresolution',0.0025,'figures',0);
    F2 = [];
    Pxx2 = [];
    
    %Determine peaks and wave period(s)
    [peaks1,locs1] = findpeaks(Pxx1,F1,'NPeaks',4,'SortStr','descend');
    peaks2 = [];
    locs2 = [];
    T1 = 1./locs1;
    T2 = [];
    
    figure; hold on; grid on; box on;
    plot(F1,Pxx1); plot(locs1,peaks1,'k*')
    xlabel 'Frequency [Hz]'; ylabel(['Variance density [m^2/Hz/',char(176),']'])
    title '1D spectra'
    legend('Laser 1','Laser 1 peaks','Laser 2','Laser 2 peaks')
    xlim([0 1])
    
    prompt = {'Please check the plots. Do the spectra seem fine? (y/n)'};
    title1 = 'Check plots'; %pop-up to check the plot
    dims = [1 35];
    definput = {'y'};
    opts.WindowStyle = 'Normal';
    answer = inputdlg(prompt,title1,dims,definput,opts);
    if strcmp(answer,'y')
        close gcf;
    elseif strcmp(answer,'n')
        display('Analysis stoppped. Check the smoothness of the signals')
        return
    end
    
else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Extra smoothing is not necessary if signals are loaded from        %
    %FrontVelocitiesAndOvertoppingResults structure                     %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % LS1 = movmedian(LS1, [5 5],1,'omitnan');%11 pts
    % LS1 = movmean(LS1, [10 10],1,'omitnan');%21 pts
    % LS2 = movmedian(LS2, [5 5],1,'omitnan');%11 pts
    % LS2 = movmean(LS2, [10 10],1,'omitnan');%21 pts
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Make sure run-up data is sufficiently smooth!!!
    %Plot run-up signal(s)
    figure; hold on; grid on; box on;
    plot(T,LS1); plot(T,LS2);
    xlabel 'time [s]'; ylabel 'a-level [m]';
    title 'Run-up signal(s)'
    legend('Laser 1','Laser 2')
    
    prompt = {'Please check the plots. Do the signals seem fine? (y/n)'};
    title1 = 'Check plots'; %pop-up to check the plot
    dims = [1 35];
    definput = {'y'};
    opts.WindowStyle = 'Normal';
    answer = inputdlg(prompt,title1,dims,definput,opts);
    if strcmp(answer,'y')
        close gcf;
    elseif strcmp(answer,'n')
        display('Analysis stoppped. Check the smoothness of the signals')
        return
    end
    
    %Determine spectra
    [F1,Pxx1] = spectrumsimple(MedianScanFrequency,LS1,'fresolution',0.0025,'figures',0);
    [F2,Pxx2] = spectrumsimple(MedianScanFrequency,LS2,'fresolution',0.0025,'figures',0);
    
    %Determine peaks and wave period(s)
    [peaks1,locs1] = findpeaks(Pxx1,F1,'NPeaks',4,'SortStr','descend');
    [peaks2,locs2] = findpeaks(Pxx2,F2,'NPeaks',4,'SortStr','descend');
    T1 = 1./locs1;
    T2 = 1./locs2;
    
    figure; hold on; grid on; box on;
    plot(F1,Pxx1); plot(locs1,peaks1,'k*')
    plot(F2,Pxx2); plot(locs2,peaks2,'r*')
    xlabel 'Frequency [Hz]'; ylabel(['Variance density [m^2/Hz/',char(176),']'])
    title '1D spectra'
    legend('Laser 1','Laser 1 peaks','Laser 2','Laser 2 peaks')
    xlim([0 1])
    
    prompt = {'Please check the plots. Do the spectra seem fine? (y/n)'};
    title1 = 'Check plots'; %pop-up to check the plot
    dims = [1 35];
    definput = {'y'};
    opts.WindowStyle = 'Normal';
    answer = inputdlg(prompt,title1,dims,definput,opts);
    if strcmp(answer,'y')
        close gcf;
    elseif strcmp(answer,'n')
        display('Analysis stoppped. Check the smoothness of the signals')
        return
    end
end

%% Determine angle of incidence
if isempty(LS2)==0 %only if 2 signals are supplied
    %Determine cross-spectral density
    [Pxy,Fxy] = cpsd(LS1,LS2,[],[],[],round(1./MedianScanFrequency));
    
    %Determine phases and lags
    Phase = angle(Pxy);
    Lag = Phase./(2*pi).*(1./Fxy);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %This smoothing is based on previous experience, but it might be    %
    %necessary to change it for each individual case                    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    Lag = movmedian(Lag,[10 10],'omitnan');%21 pts
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %Projected phase velocities
    cTest = DistanceBetweenScanLines./Lag;
    
    %Gravitational acceleration
    g=9.81;
    
    %Use dispersion relation of linear wave theory to determine phase
    %velocities
    for i = 1:1:length(Fxy) %iteration is necessary
        %Set of potential wave lengths
        %Note: Only works for wind wave regime (periods up to 25 s)!!!
        L = 0:0.005:1000;
        %dispersion relation
        F = @(L) ((2.*pi.*g)./L).*tanh((2.*pi.*WaterDepth)./L) - (2.*pi./(1/Fxy(i))).^2;
        id1 = find(abs(F(L)-0)==min(abs(F(L)-0))); %solve
        L1 = L(id1); %determine actual wave lengths
        k1=2*pi/L1; %determine wave numbers
        c(i) = sqrt((g/k1)*tanh(WaterDepth*k1)); %determine phase velocities
    end
    
    %Determine angles between incident and projected wave
    Alpha = acosd(c./abs(cTest'));
    Alpha(imag(Alpha)~=0)=NaN;
    
    %Determine angle of incidence spectrum (=relative to normal to dike)
    Beta = 90-Alpha;
    
    %Find IDs of peak frequencies
    for i = 1:1:length(locs1)
        id(i) = find(abs(Fxy-locs1(i))==min(abs(Fxy-locs1(i))),1);
    end
    for i = 1:1:length(locs2)
        id2(i) = find(abs(Fxy-locs2(i))==min(abs(Fxy-locs2(i))),1);
    end
    
    %Determine angles of incidence at spectral peaks
    AngleOfIncidencePeaks1 = Beta(id);
    AngleOfIncidencePeaks2 = Beta(id2);
    
    %Find Fxy = 1 Hz
    id3 = find(abs(Fxy-1)==min(abs(Fxy-1)),1);
    
    %Determine mean, median, standard deviation angle of incidence
    AngleOfIncidenceMean = mean(Beta(~isnan(Beta(1:id3))));%0-1 Hz
    AngleOfIncidenceMedian = median(Beta(~isnan(Beta(1:id3))));
    AngleOfIncidenceStd = std(Beta(~isnan(Beta(1:id3))));
    
    fig = figure; hold on; box on; grid on;
    left_color = [0 0 0];
    plot(F1,Pxx1,'b');
    plot(F2,Pxx2,'r');
    xlabel 'Frequency [Hz]'
    ylabel(['Variance density [m^2/Hz/',char(176),']'])
    yyaxis right
    xlim([0 1])
    plot(Fxy,Beta,'k--')
    plot(Fxy(id),Beta(id),'k*','LineWidth',2);
    plot(Fxy(id2),Beta(id2),'k*','LineWidth',2);
    ylabel(['Angle of incidence [',char(176),']'])
    set(fig,'defaultAxesColorOrder',[left_color; left_color]);
    box on; grid on;
    legend(['Variance density 1 [m^2/Hz/',char(176),']'],...
        ['Variance density 2 [m^2/Hz/',char(176),']'],...
        ['Angle of incidence [',char(176),']'],...
        ['Peak angle of incidence [',char(176),']'])
    
    prompt = ...
        {'Please check the plots. Do the spectra and angles of incidence seem fine? (y/n)'};
    title1 = 'Check plots'; %pop-up to check the plot
    dims = [1 35];
    definput = {'y'};
    opts.WindowStyle = 'Normal';
    answer = inputdlg(prompt,title1,dims,definput,opts);
    if strcmp(answer,'y')
        close gcf;
    elseif strcmp(answer,'n')
        display('Analysis stoppped. Check the smoothness of the signals')
        return
    end
    
else %angles of incidence cannot be calculated for only 1 laser signal
    Pxy = [];
    Fxy = [];
    Beta = [];
    AngleOfIncidencePeaks1 = [];
    AngleOfIncidencePeaks2 = [];
    AngleOfIncidenceMean = [];
    AngleOfIncidenceMedian = [];
    AngleOfIncidenceStd = [];
end

%% Generate and save output
%Combine all output into a AngleOfIncidenceResults structure
AngleOfIncidenceResults.T = T;
AngleOfIncidenceResults.DateAndHour = DateAndHour;
AngleOfIncidenceResults.WaterLevel = WaterLevel;
AngleOfIncidenceResults.BottomLevel = BottomLevel;
AngleOfIncidenceResults.DistanceBetweenScanLines = DistanceBetweenScanLines;
AngleOfIncidenceResults.zRunUpRelativeSmooth1 = LS1;
AngleOfIncidenceResults.zRunUpRelativeSmooth2 = LS2;
AngleOfIncidenceResults.SpectralFrequencies1 = F1';
AngleOfIncidenceResults.SpectralDensities1 = Pxx1;
AngleOfIncidenceResults.SpectralFrequencie2 = F2';
AngleOfIncidenceResults.SpectralDensities2 = Pxx2;
AngleOfIncidenceResults.PeakFrequencies1 = locs1;
AngleOfIncidenceResults.PeakPeriods1 = T1;
AngleOfIncidenceResults.PeakFrequencies2 = locs2;
AngleOfIncidenceResults.PeakPeriods2 = T2;
AngleOfIncidenceResults.CrossSpectralFrequencies = Fxy;
AngleOfIncidenceResults.CrossSpectralDensity = Pxy;
AngleOfIncidenceResults.AngleOfIncidenceSpectrum = Beta';
AngleOfIncidenceResults.AngleOfIncidencePeaks1 = AngleOfIncidencePeaks1;
AngleOfIncidenceResults.AngleOfIncidencePeaks2 = AngleOfIncidencePeaks2;
AngleOfIncidenceResults.AngleOfIncidenceMean = AngleOfIncidenceMean;
AngleOfIncidenceResults.AngleOfIncidenceMedian = AngleOfIncidenceMedian;
AngleOfIncidenceResults.AngleOfIncidenceStd = AngleOfIncidenceStd;

%Prompt to save data
prompt = {'Save data? (y/n):'};
title1 = 'Save data?';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    [filename1, filepath1] = uiputfile(...
        '*.mat', 'Save the wave (run-up) period and angle of incidence file:');
    FileName = fullfile(filepath1, filename1);
    save(FileName, 'AngleOfIncidenceResults', '-v7.3');
end
