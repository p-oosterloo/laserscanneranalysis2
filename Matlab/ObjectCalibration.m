function [aLaserObjectLocations,xLaserObjectLocations,deltaObjects,...
    LaserResolutions,LaserFootprints] = ObjectCalibration(AnalysisInput)
% [LaserObjectLocations,deltaObjects,LaserResolutions,...
%   LaserFootprints] = ObjectCalibration(AnalysisInput)

% read in data from laser scanner *.mat file containing one or more objects
% placed on the dry slope, and determine the locations of the calibration
% objects, the error in the location of these objects, the resolution of
% the laser at these object locations and the laser footprints at these
% object locations
%
% a-coordinate is defined as the distance along the slope
% x-coordinate is defined as the horizontal distance
% z-coordinate is defined as the height (vertically upward)
%
% Note: the *.mat file needs to contain a stationary and uniform situation,
% so no temporal or spatial changes in the dry slope and/or objects may
% occur!
%
% input:
% AnalysisInput data structure, with fields:
%   .R [R2,R3,R4,R5]      = radial distance from scanner (m) [for all echoes]
%   .StartAngle           = start angle (deg.)
%   .AngleRes             = angle resolution (deg.)
%   .ApertureAngle        = aperture angle of laser scanner (deg.)
%   .SlantAngle           = slant angle of scanner (deg.)
%   .xCoordinateLaser     = x-coordinate (horizontal) of scanner (m)
%   .aCoordinateLaser     = a-coordinate (along slope) of scanner (m)
%   .HeightLaser          = height of scanner above slope (m)
%   .SlopeAngleAtLaser    = angle of dike slope at scanner (deg.)
%   .xCoordinatesMeasured = measured x-coordinates (horizontal)
%                           (e.g. with measurement tape) (m)
%   .zCoordinatesMeasured = measured z-coordinates (vertical)
%                           (e.g. with measurement tape) (m)
%   .aCoordinatesMeasured = measured a-coordinates (along slope)
%                           (e.g. with measurement tape) (m)
%   .OrientationAngle     = angle at which scanner scans straight down
%                           (theta=0) (deg.)
%   .PoleOrientation      = orientation of laser pole, 'Vertical' or
%                          'Perpendicular'
%   .aAreaOfInterest      = a-coordinates of area of interest, relative to
%                           pole a-coordinate (e.g. [-10 10])
%   .xAreaOfInterest      = x-coordinates of area of interest, relative to
%                           pole x-coordinate (e.g. [-10 10])
%   .CorrectionAngle      = correction angle for rotation around y-axis (deg)
%   .MaxScanDistanceLaser = maximum distance that laser can scan (m)
%   .aObjectLocations     = a-coordinates (along slope) of objects (m)
%   .WidthLaserBeam       = width of laser beam at scanner (m)
%   .DivergenceLaserBeam  = divergence of laser beam (rad)
%
% output:
%   aLaserObjectLocations = a-coordinates of the calibration objects (m)
%   xLaserObjectLocations = x-coordinates of the calibration objects (m)
%   deltaObjects          = error in the location of these objects (m)
%   LaserResolutions      = resolution of the laser at the object locations (m)
%   LaserFootprints       = laser footprints at the object locations (m)
%
% Patrick Oosterlo, 12-2020
%
% Copyright 2020, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%% Load input
if isfield(AnalysisInput,'R'); R = AnalysisInput.R; end
if isfield(AnalysisInput,'R2'); R2 = AnalysisInput.R2; end
if isfield(AnalysisInput,'R3'); R3 = AnalysisInput.R3; end
if isfield(AnalysisInput,'R4'); R4 = AnalysisInput.R4; end
if isfield(AnalysisInput,'R5'); R5 = AnalysisInput.R5; end
if isfield(AnalysisInput,'StartAngle'); StartAngle = AnalysisInput.StartAngle; end
if isfield(AnalysisInput,'AngleRes'); AngleRes = AnalysisInput.AngleRes; end
if isfield(AnalysisInput,'ApertureAngle'); ApertureAngle = AnalysisInput.ApertureAngle; end
if isfield(AnalysisInput,'SlantAngle'); SlantAngle = AnalysisInput.SlantAngle; end
if isfield(AnalysisInput,'xCoordinateLaser')
    xCoordinateLaser = AnalysisInput.xCoordinateLaser; end
if isfield(AnalysisInput,'aCoordinateLaser')
    aCoordinateLaser = AnalysisInput.aCoordinateLaser; end
if isfield(AnalysisInput,'HeightLaser'); HeightLaser = AnalysisInput.HeightLaser; end
if isfield(AnalysisInput,'SlopeAngleAtLaser')
    SlopeAngleAtLaser = AnalysisInput.SlopeAngleAtLaser; end
if isfield(AnalysisInput,'xCoordinatesMeasured')
    xCoordinatesMeasured = AnalysisInput.xCoordinatesMeasured; end
if isfield(AnalysisInput,'zCoordinatesMeasured')
    zCoordinatesMeasured = AnalysisInput.zCoordinatesMeasured; end
if isfield(AnalysisInput,'aCoordinatesMeasured')
    aCoordinatesMeasured = AnalysisInput.aCoordinatesMeasured; end
if isfield(AnalysisInput,'OrientationAngle')
    OrientationAngle = AnalysisInput.OrientationAngle; end
if isfield(AnalysisInput,'PoleOrientation')
    PoleOrientation = AnalysisInput.PoleOrientation; end
if isfield(AnalysisInput,'aAreaOfInterest')
    aAreaOfInterest = AnalysisInput.aAreaOfInterest; end
if isfield(AnalysisInput,'xAreaOfInterest')
    xAreaOfInterest = AnalysisInput.xAreaOfInterest; end
if isfield(AnalysisInput,'CorrectionAngle')
    CorrectionAngle = AnalysisInput.CorrectionAngle; end
if isfield(AnalysisInput,'aObjectLocations')
    aObjectLocations = AnalysisInput.aObjectLocations; end
if isfield(AnalysisInput,'WidthLaserBeam')
    WidthLaserBeam = AnalysisInput.WidthLaserBeam; end
if isfield(AnalysisInput,'DivergenceLaserBeam')
    DivergenceLaserBeam = AnalysisInput.DivergenceLaserBeam; end
if isfield(AnalysisInput,'MaxScanDistanceLaser')
    MaxScanDistanceLaser = AnalysisInput.MaxScanDistanceLaser; end

RFinal = R;
if exist('R2','var')==1 %if all echoes were recorded, then use the maximum
    RFinal(R2>RFinal)=R2(R2>RFinal); %distance recorded of all the echoes
    RFinal(R3>RFinal)=R3(R3>RFinal); %to filter out rain, fog, et cetera
    RFinal(R4>RFinal)=R4(R4>RFinal);
    RFinal(R5>RFinal)=R5(R5>RFinal);
end
RFinal = median(RFinal,1,'omitnan');

theta1 = StartAngle(1):AngleRes:(ApertureAngle-abs(StartAngle)); %vector of scan angles
theta = theta1-OrientationAngle; %relative scan angles

clear R R2 R3 R4 R5 OrientationAngle StartAngle theta1

%% Determine the final corrected cartesian coordinates
%Depending if pole is vertical or perpendicular to slope, different
%methods are necessary

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %THIS PART FOR A PERPENDICULAR POLE MIGHT NEED TO BE UPDATED AND FIXED%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(PoleOrientation,'Perpendicular')==1 %Pole perpendicular to slope
    a = RFinal.*sind(theta); %a=along slope (approximate)
    z1 = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
    z = a.*sind(SlopeAngleAtLaser)+z1.*cosd(SlopeAngleAtLaser);
    %z=vertical (approximate)
    
    id1 = find(min(abs(a-aAreaOfInterest(1)))==abs(a-aAreaOfInterest(1)));
    id2 = find(min(abs(a-aAreaOfInterest(2)))==abs(a-aAreaOfInterest(2)));
    a = a(id1:id2);%only consider area of interest
    z = z(id1:id2);%only consider area of interest
    
    b = diff(a,1,2); %determine differences in a
    bsub = zeros(length(b(:,1)),1);
    bsub(bsub==0)=NaN;
    b = [bsub b]; %add row of NaNs to make length equal to R again
    clear bsub
    c = diff(z,1,2); %determine differences in x
    csub = zeros(length(c(:,1)),1);
    csub(csub==0)=NaN;
    c = [csub c]; %add row of NaNs to make length equal to R again
    clear csub
    Pythagoras = abs(b).^2-abs(c).^2;
    Pythagoras(Pythagoras<0)=NaN;%set negative values to NaN
    Pythagoras = sqrt(Pythagoras);
    %     Pythagoras = hypot(b,c);%determine a-coors using Pythagoras
    clear b c
    Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
    %Step difference cannot be more than difference between laser points
    %at max possible laser scan distance
    Pythagoras(isnan(Pythagoras))=0;%Set NaNs to 0
    x = cumsum(Pythagoras,2);%a-coors are the cumsum of the Pythago steps
    
    for i =1:1:length(a(:,1))
        %find id of laser x-coordinate, at this location, the a-coordinate
        %should also be zero for now (later this is shifted to the laser a-coor
        id(i,1) = find(min(abs(a(i,:)-0))==abs(a(i,:)-0),1);
        x(i,:)=x(i,:)-x(i,id(i));
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
elseif strcmp(PoleOrientation,'Vertical')==1 %Vertical pole
    x = RFinal.*sind(theta);
    z = HeightLaser - RFinal.*cosd(theta).*cosd(SlantAngle);
    
    id1 = find(min(abs(x-xAreaOfInterest(1)))==abs(x-xAreaOfInterest(1)));
    id2 = find(min(abs(x-xAreaOfInterest(2)))==abs(x-xAreaOfInterest(2)));
    x = x(id1:id2);
    z = z(id1:id2);
    
    b = diff(x,1,2);
    bsub = zeros(length(b(:,1)),1);
    bsub(bsub==0)=NaN;
    b = [bsub b];
    clear bsub
    c = diff(z,1,2);
    csub = zeros(length(c(:,1)),1);
    csub(csub==0)=NaN;
    c = [csub c];
    clear csub
    Pythagoras = hypot(b,c);
    clear b c
    Pythagoras(Pythagoras<0)=NaN;
    Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
    Pythagoras(isnan(Pythagoras))=0;
    a = cumsum(Pythagoras,2);
    
    for i =1:1:length(x(:,1))
        id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
        a(i,:)=a(i,:)-a(i,id(i));
    end
    
else
    disp('Error: Supply pole orientation!');
end

%Rotation matrix to calculate final x- and z-coors for rotation around y-axis
RotationMatrix = [cosd(CorrectionAngle) -sind(CorrectionAngle);...
    sind(CorrectionAngle) cosd(CorrectionAngle)];

x = x.*RotationMatrix(1,1)+z.*RotationMatrix(1,2);%rotated x (approx)
z = x.*RotationMatrix(2,1)+z.*RotationMatrix(2,2);%rotated z (approx)

b = diff(x,1,2);
bsub = zeros(length(b(:,1)),1);
bsub(bsub==0)=NaN;
b = [bsub b];
clear bsub
c = diff(z,1,2);
csub = zeros(length(c(:,1)),1);
csub(csub==0)=NaN;
c = [csub c];
clear csub
Pythagoras = hypot(b,c);
clear b c
Pythagoras(Pythagoras<0)=NaN;
Pythagoras(Pythagoras>deg2rad(AngleRes)*MaxScanDistanceLaser)=NaN;
Pythagoras(isnan(Pythagoras))=0;
a = cumsum(Pythagoras,2);

for i =1:1:length(x(:,1))
    id(i,1) = find(min(abs(x(i,:)-0))==abs(x(i,:)-0),1);
    a(i,:)=a(i,:)-a(i,id(i));
end

x = xCoordinateLaser + x;
a = aCoordinateLaser + a;
id = find(aCoordinateLaser==aCoordinatesMeasured);
zLaser = zCoordinatesMeasured(id);
z = zLaser + z;

%% Object calibration
%Find object, 2nd derivative <0 gives a local maximum in z
dzdz = diff(diff(z));
dzdz = [NaN,NaN,dzdz];%make sure length is equal to z again
%multiply with -1 to be able to find (maxima) peaks in 2nd derivative in next step
[~,peakLocs] = findpeaks(dzdz.*-1,'MinPeakHeight',0.02,...
    'MinPeakDistance',50,'NPeaks',4,'SortStr','descend'); %find objects
peakLocs = sort(peakLocs);

%Matlab often gives the locations just to the right of the peak instead of
%at the peak, so this is fixed here
peakLocs = peakLocs-1;

%plot a- and z-coordinates with possible object locations
figure; hold on; box on; grid on;
plot(a,z);
plot(a(peakLocs),z(peakLocs),'r*');
xlabel('a-coordinates [m]'); ylabel('z-coordinates [m]');
title 'Potential object locations'

% Prompt to mark correct object locations
prompt = {[num2str(length(peakLocs)),...
    ' locations found. Please enter the right object location number(s),',...
    ' separated with a semicolon (;). If no locations were found, enter: 0']};
title1 = 'Check plot'; %pop-up to mark the correct object location
dims = [1 40];
definput = {'1'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'1')
    ids=peakLocs(1);
    close gcf;
elseif strcmp(answer,'2')
    ids=peakLocs(2);
    close gcf;
elseif strcmp(answer,'3')
    ids=peakLocs(3);
    close gcf;
elseif strcmp(answer,'4')
    ids=peakLocs(4);
    close gcf;
elseif strcmp(answer,'1;2')
    ids=peakLocs(1:2);
    close gcf;
elseif strcmp(answer,'1;3')
    ids=peakLocs([1 3]);
    close gcf;
elseif strcmp(answer,'1;4')
    ids=peakLocs([1 4]);
    close gcf;
elseif strcmp(answer,'2;3')
    ids=peakLocs(2:3);
    close gcf;
elseif strcmp(answer,'2;4')
    ids=peakLocs([2 4]);
    close gcf;
elseif strcmp(answer,'3;4')
    ids=peakLocs(3:4);
    close gcf;
elseif strcmp(answer,'0')
    disp('Analysis stoppped: check data')
    return
end

aLaserObjectLocations = a(ids); %find object a-locations
xLaserObjectLocations = x(ids); %find object x-locations
%determine errors in object locations
deltaObjects = aLaserObjectLocations-(aCoordinateLaser+aObjectLocations);

%determine resolution of laser at these object locations
LaserResolutions = aLaserObjectLocations-a(ids-1);
%determine angles of incidence of laser at these object locations
LaserIncidenceAngles = 90-abs(theta(id1-1+ids));
%determine distance of laser beam at these object locations
LaserDistances = RFinal(:,id1-1+ids);
%determine footprints of laser at these object locations
LaserFootprints = ((WidthLaserBeam+LaserDistances.*DivergenceLaserBeam)./2)...
    ./sind(LaserIncidenceAngles)*2;

%plot x- and z-coordinates, object locations and errors
figure; hold on; box on; grid on;
plot(xCoordinatesMeasured,zCoordinatesMeasured);
plot(x,z);
plot(xLaserObjectLocations,z(ids),'k*');
xlabel('x-coor [m]'); ylabel('z-coor [m]')
legend('Measured data','Laser data','Found object locations')
if length(deltaObjects)==1
    title(['Difference in object location between lasers and measured:',...
         '\newline deltaObjects: ',num2str(deltaObjects),' m']);
else
    title(['Difference in object location between lasers and measured:',...
         '\newline deltaObjects: ',num2str(deltaObjects(1)),' m & ',...
        num2str(deltaObjects(2)),' m']);
end

% Prompt to check the results
prompt = {'Please check the plot. Do the results seem fine? (y/n)'};
title1 = 'Check plot'; %pop-up to check the plot
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    close gcf;
elseif strcmp(answer,'n')
    disp('Analysis stoppped: check data')
    return
end