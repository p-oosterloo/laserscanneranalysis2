function data = ReadSopasCsv(fname,Nmax)
% function data = ReadSopasCsv(fname);
%
% Read in data from SOPAS *.csv file of laser scanner (e.g. SICK LMS511).
% Can be used for comma-separated or semicolon-separated file.
%
% input:
% fname         = name of input file, including file extension (*.csv)
% Nmax          = maximum number of lines to read at once, dependent on
%                 computer memory (e.g. Nmax = 1e8 for 8 GB of RAM)
%
% output:
% data structure, with fields:
%   .R(time,angle)      = radial distance from scanner (m)
%                         a maximum of 5 R's can be determined, this depends
%                         on the echo setting of the laser scanner
%   .RSSI(time,angle)   = intensity of reflected signal (value of 0-255)
%                         a maximum of 5 RSSI's can be determined, this depends
%                         on the echo setting of the laser scanner
%   .T                  = time (s)
%   .DateAndHour        = time converted to datetime
%   .DateAndHourDatenum = time converted to datenum
%   .StartAngle         = start angle (deg.)
%   .AngleRes           = angle resolution (deg.)
%   .ScaleFactor        = scale factor
%   .ScaleOffset        = scale offset
%   .SerialNumber       = serial number of laser scanner
%
% NB: 7.5 times faster than using fgetl in a loop
%
% Patrick Oosterlo, 12-2020
%
% Copyright 2020, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest
%
% This file is part of LaserScannerAnalysis.
%
% LaserScannerAnalysis is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% LaserScannerAnalysis is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

%open file
fileinfo = dir(fname);
fid = fopen(fname,'rt');

%initialize main matrices
rawdata_tmp = char(ones(Nmax,1)*' ');
extra_rawdata = '';
cnt_loop = 0;
R = [];
R2 = [];
R3 = [];
R4 = [];
R5 = [];
RSSI = [];
RSSI2 = [];
RSSI3 = [];
RSSI4 = [];
RSSI5 = [];

%main loop reading and processing chunks of data
h = waitbar(0,'Loading data'); %display progress bar
while length(rawdata_tmp) == Nmax
    cnt_loop = cnt_loop + 1;
    
    %read in *csv file as string
    rawdata_tmp=fread(fid,Nmax,'*char');
    rawdata=[extra_rawdata;rawdata_tmp];
    
    %find serial number of scanner
    if cnt_loop == 1
        sns = strfind(rawdata','SN');
        if isempty(sns)==0
            data.SerialNumber = str2num(rawdata(sns(1)+3:sns(1)+10)');
            clear sns;
        end
    end
    
    %find ends of lines and remove the first line (header) from data
    if cnt_loop == 1
        eol = strfind(rawdata',newline);
        rawdata = rawdata(eol(1)+1:end);
    end %remove first line of file
    rawdata(rawdata==',') = ' '; %remove commas if comma separated file
    rawdata(rawdata==';') = ' '; %remove semicolons if semicolon separated
    
    %find beginning and end of distance R or reflectance RSSI data for each line
    bod = strfind(rawdata', '"DIST1"' )+7; %beginning of data-line
    eod1 = strfind(rawdata', '"DIST2"' )-1; %end of data-line
    bod2 = strfind(rawdata', '"DIST2"' )+7; %begin of data-line
    eod2 = strfind(rawdata', '"DIST3"' )-1; %end of data-line
    bod3 = strfind(rawdata', '"DIST3"' )+7; %begin of data-line
    eod3 = strfind(rawdata', '"DIST4"' )-1; %end of data-line
    bod4 = strfind(rawdata', '"DIST4"' )+7; %begin of data-line
    eod4 = strfind(rawdata', '"DIST5"' )-1; %end of data-line
    bod5 = strfind(rawdata', '"DIST5"' )+7; %begin of data-line
    eod = strfind(rawdata', '"RSSI1"' )-1; %end of data-line
    bor = strfind(rawdata', '"RSSI1"' )+7; %begin of data-line
    eor1 = strfind(rawdata', '"RSSI2"' )-1; %end of data-line
    bor2 = strfind(rawdata', '"RSSI2"' )+7; %begin of data-line
    eor2 = strfind(rawdata', '"RSSI3"' )-1; %end of data-line
    bor3 = strfind(rawdata', '"RSSI3"' )+7; %begin of data-line
    eor3 = strfind(rawdata', '"RSSI4"' )-1; %end of data-line
    bor4 = strfind(rawdata', '"RSSI4"' )+7; %begin of data-line
    eor4 = strfind(rawdata', '"RSSI5"' )-1; %end of data-line
    bor5 = strfind(rawdata', '"RSSI5"' )+7; %begin of data-line
    eol = strfind(rawdata', newline ); %find end of line = char(10) = newline
    eop = strfind(rawdata', '"FDIN"' );%in case RSSI is not last parameter in file
    
    if isempty(eop)==0
        doublefdin = find(diff(eop)<=100)+1; %find double occurances of "FDIN" in 1 line
        eop(doublefdin) = []; %in eop this is id+1, and remove the double value from vector
        while length(eop)<length(eol)
            missingfdin = eol(1:length(eop))-eop; %find missing fdins at end of lines
            missingfdinloc = find(missingfdin<0); %find missing fdins at end of lines
            if isempty(missingfdinloc)==1
                eol = eol(1:length(eop)); %remove last line of file if no fdin
            else %occurs on last line, otherwise an error occurs
                eop(missingfdinloc(1)+1:end+1) = eop(missingfdinloc(1):end); %split vector
                eop(missingfdinloc(1)) = NaN; %and insert NaN at missing fdin location
            end
        end %do this until there are no more double or missing fdin values
    end
    
    %select data and convert from strings to numbers
    N = length(R);
    for cnt = 1:length(eol)
        if cnt == 1 %find begin and end of dates and times
            bot = 1;
            eot = strfind(rawdata(1:10)',' ') - 1;
            eott = strfind(rawdata(1:100)',' ');
            eott = eott(3)-1;
        else
            bot = (eol(cnt-1))+1;
            eot = bot + strfind(rawdata(bot+(0:10))',' ') - 1;
            eott = bot + strfind(rawdata(bot+(0:100))',' ');
            eott = eott(3)-2;
        end
        
        %get dates and times in different formats
        t(N+cnt,1) = str2num(rawdata(bot:eot)');
        t2(N+cnt,1) = datetime(rawdata(eott-22:eott)',...
            'InputFormat','yyyy-MM-dd HH:mm:ss.SSS');
        t3(N+cnt,1) = datenum(rawdata(eott-22:eott)','yyyy-mm-dd HH:MM:SS.FFF');
        
        %get R (distance) values, of 1 to 5 echoes
        if isempty(eod1)==1
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(R(N+cnt-1,:)) == length(str2num(rawdata((bod(cnt)):(eod(cnt)))'))
                    R(N+cnt,:) = str2num(rawdata((bod(cnt)):(eod(cnt)))');
                elseif length(R(N+cnt-1,:)) < length(str2num(rawdata((bod(cnt)):(eod(cnt)))'))
                    tempR = str2num(rawdata((bod(cnt)):(eod(cnt)))');
                    R(N+cnt,:) = tempR(1:end-1);
                    clear tempR %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    R(N+cnt,:) = [str2num(rawdata((bod(cnt)):(eod(cnt)))') , 0];
                end
            else %check if each data line is equally long, otherwise remove last digit
                R(N+cnt,:) = str2num(rawdata((bod(cnt)):(eod(cnt)))');
            end
        else
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(R(N+cnt-1,:)) == length(str2num(rawdata((bod(cnt)):(eod1(cnt)))'))
                    R(N+cnt,:) = str2num(rawdata((bod(cnt)):(eod1(cnt)))');
                elseif length(R(N+cnt-1,:)) < length(str2num(rawdata((bod(cnt)):(eod1(cnt)))'))
                    tempR = str2num(rawdata((bod(cnt)):(eod1(cnt)))');
                    R(N+cnt,:) = tempR(1:end-1);
                    clear tempR %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    R(N+cnt,:) = [str2num(rawdata((bod(cnt)):(eod1(cnt)))') , 0];
                end
            else %check if each data line is equally long, otherwise remove last digit
                R(N+cnt,:) = str2num(rawdata((bod(cnt)):(eod1(cnt)))');
            end
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(R2(N+cnt-1,:)) == length(str2num(rawdata((bod2(cnt)):(eod2(cnt)))'))
                    R2(N+cnt,:) = str2num(rawdata((bod2(cnt)):(eod2(cnt)))');
                elseif length(R2(N+cnt-1,:)) < length(str2num(rawdata((bod2(cnt)):(eod2(cnt)))'))
                    tempR2 = str2num(rawdata((bod2(cnt)):(eod2(cnt)))');
                    R2(N+cnt,:) = tempR2(1:end-1);
                    clear tempR2 %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    R2(N+cnt,:) = [str2num(rawdata((bod2(cnt)):(eod2(cnt)))') , 0];
                end
            else %check if each data line is equally long, otherwise remove last digit
                R2(N+cnt,:) = str2num(rawdata((bod2(cnt)):(eod2(cnt)))');
            end
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(R3(N+cnt-1,:)) == length(str2num(rawdata((bod3(cnt)):(eod3(cnt)))'))
                    R3(N+cnt,:) = str2num(rawdata((bod3(cnt)):(eod3(cnt)))');
                elseif length(R3(N+cnt-1,:)) < length(str2num(rawdata((bod3(cnt)):(eod3(cnt)))'))
                    tempR3 = str2num(rawdata((bod3(cnt)):(eod3(cnt)))');
                    R3(N+cnt,:) = tempR3(1:end-1);
                    clear tempR3 %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    R3(N+cnt,:) = [str2num(rawdata((bod3(cnt)):(eod3(cnt)))') , 0];
                end
            else %check if each data line is equally long, otherwise remove last digit
                R3(N+cnt,:) = str2num(rawdata((bod3(cnt)):(eod3(cnt)))');
            end
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(R4(N+cnt-1,:)) == length(str2num(rawdata((bod4(cnt)):(eod4(cnt)))'))
                    R4(N+cnt,:) = str2num(rawdata((bod4(cnt)):(eod4(cnt)))');
                elseif length(R4(N+cnt-1,:)) < length(str2num(rawdata((bod4(cnt)):(eod4(cnt)))'))
                    tempR4 = str2num(rawdata((bod4(cnt)):(eod4(cnt)))');
                    R4(N+cnt,:) = tempR4(1:end-1);
                    clear tempR4 %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    R4(N+cnt,:) = [str2num(rawdata((bod4(cnt)):(eod4(cnt)))') , 0];
                end
            else %check if each data line is equally long, otherwise remove last digit
                R4(N+cnt,:) = str2num(rawdata((bod4(cnt)):(eod4(cnt)))');
            end
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(R5(N+cnt-1,:)) == length(str2num(rawdata((bod5(cnt)):(eod(cnt)))'))
                    R5(N+cnt,:) = str2num(rawdata((bod5(cnt)):(eod(cnt)))');
                elseif length(R5(N+cnt-1,:)) < length(str2num(rawdata((bod5(cnt)):(eod(cnt)))'))
                    tempR5 = str2num(rawdata((bod5(cnt)):(eod(cnt)))');
                    R5(N+cnt,:) = tempR5(1:end-1);
                    clear tempR5 %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    R5(N+cnt,:) = [str2num(rawdata((bod5(cnt)):(eod(cnt)))') , 0];
                end
            else %check if each data line is equally long, otherwise remove last digit
                R5(N+cnt,:) = str2num(rawdata((bod5(cnt)):(eod(cnt)))');
            end
        end
        
        %get RSSI values, of 1 to 5 echoes
        if isempty(eor1)==1
            if isempty(eop)==0 %replace end of line with end RSSI data, in case RSSI is not last param.
                if abs(eol(cnt)-eop(cnt))<=100 %small difference in end line and start other param.
                    %makes sure that the other parameter is still on the same line
                    if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                        if length(RSSI(N+cnt-1,:)) == length(str2num(rawdata((bor(cnt)):(eop(cnt)-1))'))
                            RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eop(cnt)-1))');
                        elseif length(RSSI(N+cnt-1,:)) < length(str2num(rawdata((bor(cnt)):(eop(cnt)-1))'))
                            tempRSSI = str2num(rawdata((bor(cnt)):(eop(cnt)-1))');
                            RSSI(N+cnt,:) = tempRSSI(1:end-1);
                            clear tempRSSI %this is necessary for interlaced laser scanning
                        else %this is necessary for interlaced laser scanning
                            RSSI(N+cnt,:) = [str2num(rawdata((bor(cnt)):(eop(cnt)-1))') , 0];
                        end
                    else
                        RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eop(cnt)-1))');
                    end
                else
                    if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                        if length(RSSI(N+cnt-1,:)) == length(str2num(rawdata((bor(cnt)):(eol(cnt)-1))'))
                            RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eol(cnt)-1))');
                        elseif length(RSSI(N+cnt-1,:)) < length(str2num(rawdata((bor(cnt)):(eol(cnt)-1))'))
                            tempRSSI = str2num(rawdata((bor(cnt)):(eol(cnt)-1))');
                            RSSI(N+cnt,:) = tempRSSI(1:end-1);
                            clear tempRSSI %this is necessary for interlaced laser scanning
                        else %this is necessary for interlaced laser scanning
                            RSSI(N+cnt,:) = [str2num(rawdata((bor(cnt)):(eol(cnt)-1))') , 0];
                        end
                    else
                        RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eol(cnt)-1))');
                    end
                end
            else
                if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                    if length(RSSI(N+cnt-1,:)) == length(str2num(rawdata((bor(cnt)):(eol(cnt)-1))'))
                        RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eol(cnt)-1))');
                    elseif length(RSSI(N+cnt-1,:)) < length(str2num(rawdata((bor(cnt)):(eol(cnt)-1))'))
                        tempRSSI = str2num(rawdata((bor(cnt)):(eol(cnt)-1))');
                        RSSI(N+cnt,:) = tempRSSI(1:end-1);
                        clear tempRSSI %this is necessary for interlaced laser scanning
                    else %this is necessary for interlaced laser scanning
                        RSSI(N+cnt,:) = [str2num(rawdata((bor(cnt)):(eol(cnt)-1))') , 0];
                    end
                else
                    RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eol(cnt)-1))');
                end
            end
        else
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(RSSI(N+cnt-1,:)) == length(str2num(rawdata((bor(cnt)):(eor1(cnt)))'))
                    RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eor1(cnt)))');
                elseif length(RSSI(N+cnt-1,:)) < length(str2num(rawdata((bor(cnt)):(eor1(cnt)))'))
                    tempRSSI = str2num(rawdata((bor(cnt)):(eor1(cnt)))');
                    RSSI(N+cnt,:) = tempRSSI(1:end-1);
                    clear tempRSSI %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    RSSI(N+cnt,:) = [str2num(rawdata((bor(cnt)):(eor1(cnt)))') , 0];
                end
            else
                RSSI(N+cnt,:) = str2num(rawdata((bor(cnt)):(eor1(cnt)))');
            end
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(RSSI2(N+cnt-1,:)) == length(str2num(rawdata((bor2(cnt)):(eor2(cnt)))'))
                    RSSI2(N+cnt,:) = str2num(rawdata((bor2(cnt)):(eor2(cnt)))');
                elseif length(RSSI2(N+cnt-1,:)) < length(str2num(rawdata((bor2(cnt)):(eor2(cnt)))'))
                    tempRSSI2 = str2num(rawdata((bor2(cnt)):(eor2(cnt)))');
                    RSSI2(N+cnt,:) = tempRSSI2(1:end-1);
                    clear tempRSSI2 %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    RSSI2(N+cnt,:) = [str2num(rawdata((bor2(cnt)):(eor2(cnt)))') , 0];
                end
            else
                RSSI2(N+cnt,:) = str2num(rawdata((bor2(cnt)):(eor2(cnt)))');
            end
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(RSSI3(N+cnt-1,:)) == length(str2num(rawdata((bor3(cnt)):(eor3(cnt)))'))
                    RSSI3(N+cnt,:) = str2num(rawdata((bor3(cnt)):(eor3(cnt)))');
                elseif length(RSSI3(N+cnt-1,:)) < length(str2num(rawdata((bor3(cnt)):(eor3(cnt)))'))
                    tempRSSI3 = str2num(rawdata((bor3(cnt)):(eor3(cnt)))');
                    RSSI3(N+cnt,:) = tempRSSI3(1:end-1);
                    clear tempRSSI3 %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    RSSI3(N+cnt,:) = [str2num(rawdata((bor3(cnt)):(eor3(cnt)))') , 0];
                end
            else
                RSSI3(N+cnt,:) = str2num(rawdata((bor3(cnt)):(eor3(cnt)))');
            end
            if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                if length(RSSI4(N+cnt-1,:)) == length(str2num(rawdata((bor4(cnt)):(eor4(cnt)))'))
                    RSSI4(N+cnt,:) = str2num(rawdata((bor4(cnt)):(eor4(cnt)))');
                elseif length(RSSI4(N+cnt-1,:)) < length(str2num(rawdata((bor4(cnt)):(eor4(cnt)))'))
                    tempRSSI4 = str2num(rawdata((bor4(cnt)):(eor4(cnt)))');
                    RSSI4(N+cnt,:) = tempRSSI4(1:end-1);
                    clear tempRSSI4 %this is necessary for interlaced laser scanning
                else %this is necessary for interlaced laser scanning
                    RSSI4(N+cnt,:) = [str2num(rawdata((bor4(cnt)):(eor4(cnt)))') , 0];
                end
            else
                RSSI4(N+cnt,:) = str2num(rawdata((bor4(cnt)):(eor4(cnt)))');
            end
            if isempty(eop)==0 %replace end of line with end RSSI data, in case RSSI is not last param.
                if abs(eol(cnt)-eop(cnt))<=100 %small difference in end line and start other param.
                    %makes sure that the other parameter is still on the same line
                    if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                        if length(RSSI5(N+cnt-1,:)) == length(str2num(rawdata((bor5(cnt)):(eop(cnt)-1))'))
                            RSSI5(N+cnt,:) = str2num(rawdata((bor5(cnt)):(eop(cnt)-1))');
                        elseif length(RSSI5(N+cnt-1,:)) < length(str2num(rawdata((bor5(cnt)):(eop(cnt)-1))'))
                            tempRSSI5 = str2num(rawdata((bor5(cnt)):(eop(cnt)-1))');
                            RSSI5(N+cnt,:) = tempRSSI5(1:end-1);
                            clear tempRSSI5 %this is necessary for interlaced laser scanning
                        else %this is necessary for interlaced laser scanning
                            RSSI5(N+cnt,:) = [str2num(rawdata((bor5(cnt)):(eop(cnt)-1))') , 0];
                        end
                    else
                        RSSI5(N+cnt,:) = str2num(rawdata((bor5(cnt)):(eop(cnt)-1))');
                    end
                else
                    if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                        if length(RSSI5(N+cnt-1,:)) == length(str2num(rawdata((bor5(cnt)):(eol(cnt)-1))'))
                            RSSI5(N+cnt,:) = str2num(rawdata((bor5(cnt)):(eol(cnt)-1))');
                        elseif length(RSSI5(N+cnt-1,:)) < length(str2num(rawdata((bor5(cnt)):(eol(cnt)-1))'))
                            tempRSSI5 = str2num(rawdata((bor5(cnt)):(eol(cnt)-1))');
                            RSSI5(N+cnt,:) = tempRSSI5(1:end-1);
                            clear tempRSSI5 %this is necessary for interlaced laser scanning
                        else %this is necessary for interlaced laser scanning
                            RSSI5(N+cnt,:) = [str2num(rawdata((bor5(cnt)):(eol(cnt)-1))') , 0];
                        end
                    else
                        RSSI5(N+cnt,:) = str2num(rawdata((bor5(cnt)):(eol(cnt)-1))');
                    end
                end
            else
                if N+cnt>1 %check if each data line is equally long, otherwise remove last digit
                    if length(RSSI5(N+cnt-1,:)) == length(str2num(rawdata((bor5(cnt)):(eol(cnt)-1))'))
                        RSSI5(N+cnt,:) = str2num(rawdata((bor5(cnt)):(eol(cnt)-1))');
                    elseif length(RSSI5(N+cnt-1,:)) < length(str2num(rawdata((bor5(cnt)):(eol(cnt)-1))'))
                        tempRSSI5 = str2num(rawdata((bor5(cnt)):(eol(cnt)-1))');
                        RSSI5(N+cnt,:) = tempRSSI5(1:end-1);
                        clear tempRSSI5 %this is necessary for interlaced laser scanning
                    else %this is necessary for interlaced laser scanning
                        RSSI5(N+cnt,:) = [str2num(rawdata((bor5(cnt)):(eol(cnt)-1))') , 0];
                    end
                else
                    RSSI5(N+cnt,:) = str2num(rawdata((bor5(cnt)):(eol(cnt)-1))');
                end
            end
            
        end
    end
    data.ScaleFactor = R(1,1);
    data.ScaleOffset = R(1,2);
    data.StartAngle  = R(:,3)./10000; %might change in time with interlaced scanning, therefore use whole column
    data.AngleRes    = R(1,4)./10000;
    %
    extra_rawdata = rawdata(eol(end)+1:end);
    waitbar(cnt_loop*Nmax/fileinfo.bytes,h)
end
close(h);
fclose(fid);

%process and cut R and RSSI
R(R==0) = nan; %wrong measurements give R = 0, so set to NaN;
R2(R2==0) = nan; %wrong measurements give R = 0, so set to NaN;
R3(R3==0) = nan; %wrong measurements give R = 0, so set to NaN;
R4(R4==0) = nan; %wrong measurements give R = 0, so set to NaN;
R5(R5==0) = nan; %wrong measurements give R = 0, so set to NaN;
data.R = R(:,5:end)./1000; %convert to metre
data.R2 = R2(:,5:end)./1000;
data.R3 = R3(:,5:end)./1000;
data.R4 = R4(:,5:end)./1000;
data.R5 = R5(:,5:end)./1000;
data.RSSI = RSSI(:,5:end);
data.RSSI2 = RSSI2(:,5:end);
data.RSSI3 = RSSI3(:,5:end);
data.RSSI4 = RSSI4(:,5:end);
data.RSSI5 = RSSI5(:,5:end);
data.T = t./1000; %convert to seconds
data.DateAndHour = t2;
data.DateAndHourDatenum = t3;

%Show data saving prompt/window
prompt = {'Save data to *.mat file? (y/n):'};
title1 = 'Save data?';
dims = [1 35];
definput = {'y'};
opts.WindowStyle = 'Normal';
answer = inputdlg(prompt,title1,dims,definput,opts);
if strcmp(answer,'y')
    [filename, filepath] = uiputfile('*.mat', 'Save the file:');
    FileName = fullfile(filepath, filename);
    save(FileName, 'data', '-v7.3');
end