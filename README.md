**LaserScannerAnalysis

Data processing and data analysis of laser scanner system using two SICK LMS511 laser scanners.

Copyright 2020, Patrick Oosterlo, Delft University of Technology, Waterschap Noorderzijlvest**

This file is part of LaserScannerAnalysis.

LaserScannerAnalysis is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LaserScannerAnalysis is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LaserScannerAnalysis. If not, see <https://www.gnu.org/licenses/>.

----------------------------------------------------------------------------------------------------------

% Steps below are based on Matlab. A GUI and Python versions of the scripts will be added in the future.
% File run_analysis_laserdata contains all analysis steps and further explanations

%% Run through all the steps of the analysis of the laser scanner data, in this case SICK LMS511 laser scanners using SOPAS ET

%% Step 1: Convert SOPAS *.data file to *.csv file within SOPAS
% 1. Open *.data file in SOPAS data recorder
% 2. Press the export button
% 3. Export only selected variables (2 times ScanData) or certain time range
% 4. Export to *.csv, create own file for each variable, comma or semicolon separated and period decimal separator

%% Step 2: Load *.csv file and convert relevant data to *.mat file
% Makes use of ReadSopasCsv
% Enter filename and Nmax, the max. number of lines (dependent on pc memory)

%% Step 3: Determine dry dike slope
% No 'automatic' script exists for this step, this is more manual labour.
% A similar procedure as the file DetermineDrySlope could be used:
% 1. Generate *.mat files with the dry slope at low tides (so recorded outside the storm peaks)
% 2. Load these files and apply median filters on the data
% 3. Merge the data of different low tides and determine the median values
% 4. Determine the maximum differences between the different dry slopes
% 5. Only for Ciara: remove the influence of the painted grid lines
% 6. Save the data

%% Step 4: Determine orientation angle of laser
% Makes use of DetermineOrientationAngle
% At which scan angle does the laser scan straight down (theta=0)?
% 1. Load calibration file for laser 1 or 2, this file should have an object on the slope, directly under the laser scanner!
% 2. Prepare input and determine orientation angle

%% Step 5: First part of dike slope calibration
% Makes use of SlopeCalibration
% Determine differences between GPS-measured dike slope and laser slope
% Determine CorrectionAngle for rotation of laser line around the y-axis
% 1. Load dry slope file for laser 1 or 2, this file should have no objects on the slope!
% 2. Prepare input and determine correction angle

%% Step 6: Second part of dike slope calibration
% Makes use of ObjectCalibration
% Calibration with object on slope at known location
% 1. Load file for scanner 1 or 2, containing one or more objects placed on the dry slope
% 2. Prepare input and calibrate slope

%% Step 7: Determine layer thickness and run-up
% Makes use of AnalyseRunUp
% 1. Load file with run-up for scanner 1 or 2
% 2. Prepare input and determine layer thickness and run-up
% Options for improving the run-up results:
% a: Cut upper part of slope from data (AreaOfInterest)
% b: Adjust rain/person filter to smaller number of NaNs (NaNMax)
% c: Adjust layer thickness threshold (last resort)

%% Step 8: Determine front velocities, overtopping volumes and discharges
% Makes use of AnalyseFrontVelocitiesOvertopping
% 1. Load RunUpResults *.mat file for laser scanner 1 or 2
% 2. Prepare input and determine front velocity, overtopping volumes & discharges
% Options for improving the front velocity and overtopping results:
% a: Toggle removal of bias in layer thickness on/off
% b: Smoothen run-up and/or layer thickness signals
% c: Adjust layer thickness threshold (last resort)

%% Step 9: Determine wave (run-up) peak period and angle of incidence
% Makes use of AnalyseAngleOfIncidence
% 1. Load FrontVelocitiesAndOvertoppingResults *.mat file for laser scanner 1 or two files, for laser scanner 1 and 2. 
%    The angle of incidence can only be determined based on 2 laser run-up signals.
% 2. Prepare input and determine wave (run-up) peak period and angle of incidence
% Options for improving the front velocity and overtopping results:
% a: Smoothen run-up and/or layer thickness signals
% b: Edit smoothing of time lag spectrum within analysis script (last resort)
